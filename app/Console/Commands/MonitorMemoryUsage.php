<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MonitorMemoryUsage extends Command
{
    protected $signature = 'monitor:memory';
    protected $description = 'Monitor memory usage while running route cache command';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        ini_set('memory_limit', '512M');

        $initialUsage = memory_get_usage();
        $this->info("Initial memory usage: " . $initialUsage . " bytes");

        // Run the route:cache command
        $this->call('route:cache');

        $finalUsage = memory_get_usage();
        $this->info("Final memory usage: " . $finalUsage . " bytes");

        $peakUsage = memory_get_peak_usage();
        $this->info("Peak memory usage: " . $peakUsage . " bytes");
    }
}
