<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\User;
use App\Models\ScheduledOrder;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Jobs\CheckRemindingTime;
use App\Jobs\CheckShippingTime;
class ScheduleTomorrowOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Schedule tomorrow orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $periodicOrders = DB::select("SELECT * FROM orders WHERE order_type = 'periodic'");

        foreach ($periodicOrders as $order) {
            $orderDate = "";
            $remindDate = "";
            if($order->frequency == 'daily'){
                
                $orderDate  = Carbon::now()->startOfDay()->addDays(1);
                $remindDate = $orderDate->copy()->subHours(12);
                $currentDate = Carbon::now();
                $flag = true;

            } elseif($order->frequency == 'days_of_week'){
                $days = json_decode($order->days);
                foreach($days as $day){
                    $daysUntilTargetDay = abs( $day - $currentDate->dayOfWeek);
                    if($day >= $currentDate->dayOfWeek){
                        $orderDate = Carbon::now()->addDays($daysUntilTargetDay)->startOfDay();
                    }else{
                        $orderDate = Carbon::now()->subDays($daysUntilTargetDay)->startOfDay();
                    }

                    $currentDate = Carbon::now();
                    $remindDate = $orderDate->copy()->subHours(12);
                    $flag = $currentDate->isSameDay($remindDate);
                    if($flag) break;
                }

            } elseif($order->frequency == 'days_of_month'){

                $days = json_decode($order->days);
                foreach($days as $day){ 
                    $orderDate = Carbon::now()
                    ->startOfMonth()
                    ->addDays($day - 1)
                    ->startOfDay();
                    $remindDate = $orderDate->copy()->subHours(12);
                    $currentDate = Carbon::now();
                    $flag = $currentDate->isSameDay($remindDate);;
                    if($flag) break;
                }
            }
            if($flag)
            {
                $scheduledOrder = ScheduledOrder::create([
                    'order_id' => $order->id,
                    'datetime' => $orderDate,
                    ]);
                $result  = DB::select("SELECT * FROM orders WHERE id = $order->id");
                $periodicOrder = reset($result);

                $user = User::FindOrFail($periodicOrder->user_id);
                CheckRemindingTime::dispatch($scheduledOrder, $user)->delay($remindDate);
                CheckShippingTime::dispatch($scheduledOrder)->delay($orderDate);
            }
        }
    }
}
