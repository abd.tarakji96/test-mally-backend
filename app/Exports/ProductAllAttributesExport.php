<?php

namespace App\Exports;

use App\Models\Product;
use App\Models\ProductStock;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProductAllAttributesExport implements FromQuery, WithHeadings, WithMapping
{
    
    public function query()
    {
        $product_attributes = ProductStock::where('id' ,'!=', 0);
        return $product_attributes;
    }

    public function map($product_attribute): array
    {
        if($product_attribute->product != null)
            return [
                $product_attribute->id,
                $product_attribute->product->sl,
                $product_attribute->product->finex_number,
                $product_attribute->product->productLanguages[0]->name,
                $product_attribute->product->productLanguages[0]->short_description,
                $product_attribute->product->productLanguages[0]->description,
                $product_attribute->sku,
                $product_attribute->current_stock,
                $product_attribute->price,
            ];
        return [];
    }

    public function headings(): array
    {
        return [
            'id',
            'sl',
            'finex_number',
            'name',
            'short_description',
            'description',
            'sku',
            'current stock',
            'price',
        ];
    }
}
