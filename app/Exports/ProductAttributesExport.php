<?php

namespace App\Exports;

use App\Models\Product;
use App\Models\ProductStock;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProductAttributesExport implements FromQuery, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }
    
    public function query()
    {
        $product = $this->product;
        $product_attributes = ProductStock::where('product_id', $product->id);
        return $product_attributes;
    }

    public function map($product_attribute): array
    {

        return [
            $product_attribute->id,
            $product_attribute->product->sl,
            $product_attribute->product->finex_number,
            $product_attribute->product->productLanguages[0]->name,
            $product_attribute->product->productLanguages[0]->short_description,
            $product_attribute->product->productLanguages[0]->description,
            $product_attribute->sku,
            $product_attribute->current_stock,
            $product_attribute->price,
        ];
    }

    public function headings(): array
    {
        return [
            'id',
            'sl',
            'finex_number',
            'name',
            'short_description',
            'description',
            'sku',
            'current stock',
            'price',
        ];
    }
}
