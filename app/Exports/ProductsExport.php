<?php

namespace App\Exports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Http\Request;
use App\Models\SellerProfile;
use App\Models\Media;
use Maatwebsite\Excel\Concerns\WithMapping;


class ProductsExport implements FromQuery, WithHeadings, WithMapping
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function query()
    {
        $req = $this->request;



        $products = Product::with('createdBy', 'productLanguages', 'user', 'stock', 'category', 'brand')
            ->where('is_deleted', 0)
            ->CheckSellerSystem()
            ->whereHas('productLanguages', function ($q) use ($req) {
                $q->where('name', 'like', '%' . $req->q . '%');
                $q->orWhere('tags', 'like', '%' . $req->q . '%');
                $q->orwhere('description', 'like', '%' . $req->q . '%');
                $q->orwhere('short_description', 'like', '%' . $req->q . '%');
                $q->orwhere('meta_title', 'like', '%' . $req->q . '%');
                $q->orwhere('meta_description', 'like', '%' . $req->q . '%');
                $q->orwhere('meta_keywords', 'like', '%' . $req->q . '%');
            })
            ->when($req->status == 'trash' && $req->status != null, function ($query) {
                $query->onlyTrashed();
            })
            ->when($req->status != 'trash' && $req->status != 'pending' && $req->status != null, function ($query) use ($req) {
                $query->where('status', $req->status)->where('is_approved', 1);
            })
            ->when($req->status == 'pending', function ($query) {
                $query->where('is_approved', 0);
            })
            ->when($req->sq != null, function ($q) use ($req) {
                $q->where('user_id', $req->sq);
            })
            ->when(!addon_is_activated('wholesale'), function ($q) {
                $q->where('is_wholesale', 0);
            })
            ->when($req->product_for != '', function ($q) use ($req) {
                $q->when($req->product_for == 'admin', function ($for) {
                    $for->where('user_id', 1);
                });
                $q->when($req->product_for == 'seller', function ($for) {
                    $for->where('user_id', '!=', 1);
                });
                $q->when($req->product_for == 'digital', function ($for) {
                    $for->where('is_digital', 1);
                });
                $q->when($req->product_for == 'catalog', function ($for) {
                    $for->where('is_catalog', 1);
                });
                $q->when($req->product_for == 'classified', function ($for) {
                    $for->where('is_classified', 1);
                });
                $q->when($req->product_for == 'wholesale', function ($for) {
                    $for->where('is_wholesale', 1);
                });
            })
            ->when($req->sl != null, function ($query) use ($req) {
                $seller = SellerProfile::find($req->sl);
                $query->where('user_id', $seller->user_id);
            })
            ->when($req->c != null, function ($q) use ($req) {
                $category_ids = \App\Utility\CategoryUtility::getMyAllChildIds($req->c);
                $category_ids[] = (int)$req->c;
                $q->whereIn('category_id', $category_ids);
            });
        $sorting = $req->s;
        switch ($sorting) {
            case 'latest_on_top':
                $products->orderByDesc('id');
                break;
            case 'oldest_on_top':
                $products->orderBy('id');
                break;
            case 'sale_high':
                $products->orderByDesc('total_sale');
                break;
            case 'sale_low':
                $products->orderBy('total_sale');
                break;
            case 'rating_high':
                $products->orderByDesc('rating');
                break;
            case 'rating_low':
                $products->orderBy('rating');
                break;
            default:
                $products->orderBy('id', 'desc');
                break;
        };
        return $products;
    }

    public function map($product): array
    {
        return [
            $product->id,
            $product->sl,
            $product->getTranslation('name', 'ar'),
            $product->brand?->translate->title,
            $product->category?->getTranslation('title', 'ar'),
            $product->user?->first_name . ' ' . $product->user?->last_name,
            $product->created_by,
            $product->slug,
            $product->price,
            $product->barcode,
            $product->colors,
            $product->attribute_sets,
            $product->selected_variants,
            $product->selected_variants_ids,
            $product->current_stock,
            $product->minimum_order_quantity,
            $product->total_sale,
            $product->status,
            $product->rating,
            $product->viewed,
            $product->reward,
        ];
    }

    public function headings(): array
    {
        return [
            'id',
            'SL',
            'name',
            'brand',
            'category',
            'user',
            'created_by',
            'slug',
            'price',
            'barcode',
            'colors',
            'attribute_sets',
            'selected_variants',
            'selected_variants_ids',
            'current_stock',
            'minimum_order_quantity',
            'total_sale',
            'status',
            'rating',
            'viewed',
            'reward'
        ];
    }
}
