<?php

namespace App\Exports;

use App\Models\SellerProfile;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SellerExport implements FromQuery, WithHeadings, WithMapping
{

    public function query()
    {
        $sellers = SellerProfile::where('id', '!=' ,0);
        return $sellers;
    }

    public function map($seller): array
    {
        return [
            $seller->id,
            $seller->user->first_name,
            $seller->user->last_name,
            $seller->user->phone,
            $seller->user->email,
            $seller->shop_name,
            $seller->slug,
            $seller->phone_no,
            $seller->address,
        ];
    }

    public function headings(): array
    {
        return [
            'id',
            'first_name',
            'last_name',
            'phone',
            'email',
            'shop_name',
            'slug',
            'phone_no',
            'address',
        ];
    }
}
