<?php

namespace App\Exports;

use App\Models\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UserExport implements FromQuery, WithHeadings, WithMapping
{

    public function query()
    {
        $users = User::where('user_type','customer');
        return $users;
    }

    public function map($user): array
    {
        return [
            $user->id,
            $user->first_name,
            $user->last_name,
            $user->phone,
            $user->wp_number,
            $user->email,
            $user->balance,
            $user->gender,
            $user->date_of_birth,
            $user->own_code,
        ];
    }

    public function headings(): array
    {
        return [
            'id',
            'first_name',
            'last_name',
            'phone',
            'whatsapp number',
            'email',
            'balance',
            'gender',
            'date of birth',
            'own code',
        ];
    }
}
