<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\Admin\LanguageInterface;
use App\Repositories\Interfaces\Admin\Product\PrivacyInterface;
use App\Repositories\Interfaces\Admin\Product\PrivacyLanguageInterface;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrivacyController extends Controller
{

    protected $privacies;
    protected $privacyLanguage;
    protected $languages;

    public function __construct(PrivacyInterface $privacies, PrivacyLanguageInterface $privacyLanguage, LanguageInterface $languages)
    {
        $this->privacies = $privacies;
        $this->privacyLanguage = $privacyLanguage;
        $this->languages = $languages;
    }


    public function index()
    {
        $privacies = $this->privacies->paginate(get_pagination('index_form_paginate'));
        return view('admin.products.privacies.index', compact('privacies'));

    }


    public function store(Request $request)
    {
        if (isDemoServer()):
            Toastr::info(__('This function is disabled in demo server.'));
            return redirect()->back();
        endif;

        DB::beginTransaction();
        try {
            $this->privacies->store($request);
            Toastr::success(__('Created Successfully'));
            DB::commit();
            return redirect()->route('privacies');
        } catch (\Exception $e) {
            DB::rollBack();
            Toastr::error($e->getMessage());
            return redirect()->back();
        }
    }


    public function edit($id, Request $request)
    {
        $languages = $this->languages->all()->orderBy('id', 'asc')->get();
        $lang = $request->lang != '' ? $request->lang : \App::getLocale();
        $r = $request->r != '' ? $request->r : $request->server('HTTP_REFERER');
        if ($privacy_language = $this->privacies->getByLang($id, $lang)) :
            return view('admin.products.privacies.update', compact('privacy_language', 'languages', 'lang', 'r'));
        else :
            Toastr::error(__('Not found'));
            return back();
        endif;
    }


    public function update(Request $request)
    {
        if (isDemoServer()):
            Toastr::info(__('This function is disabled in demo server.'));
            return redirect()->back();
        endif;

        DB::beginTransaction();
        try {
            $this->privacies->update($request);
            Toastr::success(__('Updated Successfully'), __('Success'));
            DB::commit();
            return redirect($request->r);
        } catch (\Exception $e) {
            DB::rollBack();
            Toastr::error($e->getMessage());
            return redirect()->back();
        }
    }


}
