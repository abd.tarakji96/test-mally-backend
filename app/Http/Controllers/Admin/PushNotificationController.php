<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PushNotification;
use App\Models\User;
use Illuminate\Http\Request;
use App\CPU\NotifyHelper;

class PushNotificationController extends Controller
{
    public function index()
    {
        $notifications = PushNotification::all();
        return view('admin.marketing.notifications', compact('notifications'));
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
       
        $notifications = PushNotification::all();
        $icon = settingHelper('favicon');
        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'image'       => static_asset($icon['image_57x57_url']),
        ];

        $users = User::where('is_notify',1)->get();
        foreach ($users as $user) {
            NotifyHelper::send_push_notif_to_device($user->cm_firebase_token, $data);
        }
        $not = PushNotification::create($request->all());
        $not->notification_count = count($users);
        $not ->save();
        return redirect()->route('notifications',compact('notifications'));
    }

    public function show(PushNotification $notification)
    {
        
    }

    public function edit(PushNotification $notification)
    {

    }

    public function update(Request $request, PushNotification $notification)
    {
    
    }

    public function delete($id)
    {
        $notification = PushNotification::find($id);
        $notification->delete();
        $notifications = PushNotification::all();

        return redirect()->route('notifications',compact('notifications'));
    }
}
