<?php

namespace App\Http\Controllers\Admin\Recipe;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Recipe\RecipeRequest;
use App\Models\Product;
use App\Repositories\Interfaces\Admin\Recipe\RecipeCategoryInterface;
use App\Repositories\Interfaces\Admin\Recipe\RecipeInterface;
use App\Repositories\Interfaces\Admin\LanguageInterface;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class RecipeController extends Controller
{

    protected $recipes;
    protected $languages;
    protected $categories;

    public function __construct(RecipeCategoryInterface $categories, LanguageInterface $languages, RecipeInterface $recipes)
    {
        $this->categories       = $categories;
        $this->languages        = $languages;
        $this->recipes          = $recipes;
    }
    public function index(Request $request, $status = null){
        try {
            $categories= $this->categories->all()->where('status', 1)->get();
            $posts= $this->recipes->paginate($request, $status ,get_pagination('pagination'));
            
            return view('admin.recipes.index',compact('posts','status', 'categories'));
        } catch (\Exception $e){
            Toastr::error($e->getMessage());
            return back();
        }
    }
    public function create(Request $request){
        $categories     = $this->categories->all()->where('status', 1)->get();
        $products = Product::all();
        $recipe_products=null;

        $r = $request->server('HTTP_REFERER');
        return view('admin.recipes.add-recipe',compact('categories','products','recipe_products','r'));
    }
    public function store(RecipeRequest $request){
        if (isDemoServer()):
            Toastr::info(__('This function is disabled in demo server.'));
            return redirect()->back();
        endif;

        DB::beginTransaction();
        try {
            $this->recipes->store($request);
            Toastr::success(__('Created Successfully'));
            DB::commit();

            return redirect()->route('recipes');
        } catch (\Exception $e) {
            DB::rollBack();
            Toastr::error($e->getMessage());
            return redirect()->back();
        }
    }
    public function edit(Request $request, $id){
        $languages  = $this->languages->all()->orderBy('id', 'asc')->get();
        $lang       = $request->lang != '' ? $request->lang : \App::getLocale();

        $categories     = $this->categories->all()->where('status', 1)->get();
        $post     = $this->recipes->getByLang($id, $lang);
        $products = Product::all();
        $recipe_products = $post->recipe->products;

        $r = $request->server('HTTP_REFERER');
        return view('admin.recipes.add-recipe',compact('categories','post','products','recipe_products','r','languages','lang'));
    }
    public function update(RecipeRequest $request){
        if (isDemoServer()):
            Toastr::info(__('This function is disabled in demo server.'));
            return redirect()->back();
        endif;

        DB::beginTransaction();
        try {
            $this->recipes->update($request);
            Toastr::success(__('Updated Successfully'));
            DB::commit();
            return redirect($request->r);
        } catch (\Exception $e) {
            DB::rollBack();
            Toastr::error($e->getMessage());
            return redirect()->back();
        }
    }
    public function restore($id){
        DB::beginTransaction();
        try {
            $this->recipes->restore($id);
            Toastr::success(__('Updated Successfully'));
            DB::commit();
            return redirect()->route('recipes');
        } catch (\Exception $e) {
            DB::rollBack();
            Toastr::error($e->getMessage());
            return redirect()->back();
        }
    }

    public function ajaxRecipesFilter(Request $request)
    {
        $term           = trim($request->q);
        if (empty($term)) {
            return \Response::json([]);
        }

        $recipes = $this->recipes->ajaxRecipeFilter($term);
        $formatted_recipes   = [];
        foreach($recipes as $recipe){
            $formatted_recipes[] = ['id' => $recipe->id, 'text' => $recipe->getTranslation('title', \App::getLocale())];
        }
        return \Response::json($formatted_recipes);
    }
}
