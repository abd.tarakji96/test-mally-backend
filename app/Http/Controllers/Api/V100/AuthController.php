<?php

namespace App\Http\Controllers\Api\V100;

use Twilio\Rest\Client;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\PasswordRequest;
use App\Models\RegistrationRequest;
use App\Models\User;
use App\Traits\ApiReturnFormatTrait;
use App\Traits\ImageTrait;
use App\Traits\SendMailTrait;
use App\Traits\SmsSenderTrait;
use App\Utility\AppSettingUtility;
use Carbon\Carbon;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Clarkeash\Doorman\Facades\Doorman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Sentinel;
use App\Repositories\Admin\RewardSystemRepository;

use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    use ApiReturnFormatTrait, SmsSenderTrait, ImageTrait, SendMailTrait;

    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $validator = Validator::make($request->all(), [
                'phone' => 'required|max:255',
                'password' => 'required|min:5|max:30',
            ]);

            if ($validator->fails()) {
                return $this->responseWithError(__('Required field missing'), $validator->errors(), 422);
            }

            $user = User::where('phone', $request->phone)->where('is_deleted', 0)->first();

            if (blank($user)) {
                return $this->responseWithError(__('User not found'), [], 422);
            } elseif ($user->is_user_banned == 1) {
                return $this->responseWithError(__('Your account has been banned'), [], 401);
            }



            $credentials = $request->only('phone', 'password');

            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return $this->responseWithError(__('Invalid credentials'), [], 401);
                }
            } catch (JWTException $e) {
                return $this->responseWithError(__('Unable to login, please try again'), [], 422);
            } catch (ThrottlingException $e) {
                return $this->responseWithError(__('Suspicious activity on your ip, try after') . ' ' . $e->getDelay() . ' ' . __('seconds'), [], 422);
            } catch (NotActivatedException $e) {
                return $this->responseWithError(__('Account is not activated. Verify your account first'), [], 400);
            } catch (\Exception $e) {
                return $this->responseWithError($e->getMessage(), [], 500);
            }

            try {
                if ($user->status == 0) :
                    $this->sendOTP($request);
                    return $this->responseWithError(__('Your account is not activated'), [], 403);

                elseif ($user->status == 2) :
                    return $this->responseWithError(__('Your account is suspend'), []);
                endif;
                Sentinel::authenticate($request->all());
            } catch (NotActivatedException $e) {
                return $this->responseWithError(__('Your account is not verified.Please verify your account.'), [], 500);
            }
            // $user = User::find(auth()->user()->id);
            $user->cm_firebase_token = $request->cm_firebase_token;
            $user->save();

            $data['token'] = $token;
            $data['first_name'] = $user->first_name;
            $data['last_name'] = $user->last_name;
            $data['id'] = $user->id;
            $data['image'] = $user->profile_image;
            $data['phone'] = nullCheck($user->phone);
            $data['wp_number'] = nullCheck($user->wp_number);
            $data['email'] = nullCheck($user->email);
            $data['own_code'] = nullCheck($user->own_code);
            $data['invitation_code'] = nullCheck($user->invitation_code);
            $data['socials'] = $user->socials == null ? [] : $user->socials;
            $data['date_of_birth'] = $user->date_of_birth;
            $data['gender']         = $user->gender;
            $data['min_bill']       = $user->min_bill;
            Cart::where('user_id', getWalkInCustomer()->id)->where('trx_id', $request->trx_id)->update(['user_id' => $user->id]);

            return $this->responseWithSuccess(__('Login Successfully'), $data, 200);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), [], 500);
        }
    }

    public function register(Request $request): \Illuminate\Http\JsonResponse
    {
        try {

            $validator = Validator::make($request->all(), [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'phone' => 'required|max:15',
                'wp_number' => 'required|max:15',
                'password' => 'required|min:5|max:30|confirmed',
            ]);

            if ($validator->fails()) {
                return $this->responseWithError(__('Required field missing'), $validator->errors(), 422);
            }

            // البحث عن المستخدمين غير المحذوفين بنفس الهاتف أو رقم الواتساب
            $activeUser = User::where(function ($query) use ($request) {
                $query->where('phone', $request->phone)
                    ->orWhere('wp_number', $request->wp_number);
            })->where('is_deleted', 0)->first();

            // التحقق من حالة المستخدم إذا كان موجودًا
            if ($activeUser) {
                if ($activeUser->status == 0) {
                    // المستخدم موجود ولكن حالته غير مفعلة
                    $this->sendOTP($request);

                    return response()->json([
                        'message' => __('You need to activate your account through the OTP.'),
                        'code' => 0
                    ], 200);
                } else {
                    // المستخدم موجود وحالته مفعلة
                    return response()->json([
                        'message' => __('Phone number or WhatsApp number already exists.'),
                        'code' => -1
                    ], 422);
                }
            }

            // إذا لم يكن هناك حسابات غير محذوفة، تحقق من الحسابات المحذوفة
            $deletedUsers = User::where(function ($query) use ($request) {
                $query->where('phone', $request->phone)
                    ->orWhere('wp_number', $request->wp_number);
            })->where('is_deleted', 1)->get();

            // إذا كانت هناك حسابات محذوفة، يتم إنشاء حساب جديد
            if ($deletedUsers->count() > 0) {
                $newUser = $this->createNewUser($request);
                $newUser->own_code = Doorman::generate()->unlimited()->make()[0]->code;
                $newUser->save();
                $this->sendOTP($request);

                return $this->responseWithSuccess(__('Account created successfully.'), [], 200);
            }



            $request['permissions'] = [];
            if (settingHelper('disable_email_confirmation') == 1) {
                $user = $this->createNewUser($request);
                $this->sendOTP($request);
                $msg = __('OTP Sended');
                Cart::where('user_id', getWalkInCustomer()->id)->where('trx_id', $request->trx_id)->update(['user_id' => $user->id]);
            } else {
                $user = $this->createNewUser($request);
                $activation = Activation::create($user);
                $this->sendmail($request->email, 'Registration', $user, 'email.auth.activate-account-email', url('/') . '/activation/' . $request->email . '/' . $activation->code);
                $msg = __('Check your mail to verify your account');
            }
            if (isset($request->invitation_code) && $request->invitation_code != null) {
                $users = DB::table('users')
                    ->where('phone', 'like', "%{$request->phone}%")   // Phone contains the input phone number
                    ->where('invitation_code', $request->invitation_code)  // Invitation code is an exact match
                    ->get();

                //check if users not found 
                if ($users->isEmpty()) {
                    $reward_repo = new RewardSystemRepository();
                    $reward_repo->createRewardInvitation($request->invitation_code);
                }
            }
            $user->own_code = Doorman::generate()->unlimited()->make()[0]->code;
            $user->save();

            return $this->responseWithSuccess($msg, [], 200);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), [], 500);
        }
    }

    private function sendOTP($request)
    {
        // if (addon_is_activated('otp_system')) :
        if (!$this->send('+' . $request->phone, '')) :
            return response()->json([
                'error' => __('Unable to send otp')
            ]);
        endif;
        // endif;
        RegistrationRequest::create($request->all());
    }
    // دالة لإنشاء حساب جديد
    private function createNewUser($request, $activate = true)
    {
        $userDetails = [
            'email' => $request->email,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'wp_number' => $request->wp_number,
            'password' => $request->password,
            'invitation_code' => $request->invitation_code,
            'cm_firebase_token' => $request->cm_firebase_token,
        ];

        if ($activate) {
            return Sentinel::registerAndActivate($userDetails);
        } else {
            return Sentinel::register($userDetails);
        }
    }

    public function registerByPhone(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'phone' => 'required|max:255|unique:users,phone'
            ]);

            if ($validator->fails()) {
                return $this->responseWithError(__('Required field missing'), $validator->errors(), 422);
            }

            DB::beginTransaction();

            $request['phone'] = str_replace(' ', '', $request->phone);

            $req = RegistrationRequest::where('phone', $request->phone)->first();

            if ($req && Carbon::parse($req->created_at)->addMinutes(2) >= Carbon::now()) {
                return $this->responseWithError(__('Verification Code was Already Sent'), $validator->errors(), 500);
            }

            RegistrationRequest::where('phone', $request->phone)->delete();

            $otp = rand(10000, 99999);
            if ($request->phone && addon_is_activated('otp_system')) :
                $sms_templates = AppSettingUtility::smsTemplates();
                $sms_template = $sms_templates->where('tab_key', 'signup')->first();
                $sms_body = str_replace('{otp}', $otp, @$sms_template->sms_body);
                if (!$this->send($request->phone, $sms_body, $sms_template->template_id)) :
                    return response()->json([
                        'error' => __('Unable to send otp')
                    ]);
                endif;
            endif;
            $request['otp'] = $otp;
            RegistrationRequest::create($request->all());

            DB::commit();
            return $this->responseWithSuccess(__('OTP Send Successfully'), [], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->responseWithError($e->getMessage(), [], 500);
        }
    }

    public function registerApp(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'phone' => 'required|max:255|unique:users,phone',
                'wp_number' => 'max:255|unique:users,phone',
                'email' => 'required|max:255|unique:users,email',
                'password' => 'required|min:5|max:30|confirmed',
            ]);

            if ($validator->fails()) {
                return $this->responseWithError(__('Required field missing'), $validator->errors(), 422);
            }

            DB::beginTransaction();

            $request['phone'] = str_replace(' ', '', $request->phone);

            $req = RegistrationRequest::where('phone', $request->phone)->first();

            if ($req && Carbon::parse($req->created_at)->addMinutes(2) >= Carbon::now()) {
                return $this->responseWithError(__('Verification Code was Already Sent'), $validator->errors(), 500);
            }

            RegistrationRequest::where('phone', $request->phone)->delete();

            $otp = rand(1000, 9999);
            if ($request->phone && addon_is_activated('otp_system')) :
                $sms_templates = AppSettingUtility::smsTemplates();
                $sms_template = $sms_templates->where('tab_key', 'signup')->first();
                $sms_body = str_replace('{otp}', $otp, @$sms_template->sms_body);
                if (!$this->send($request->phone, $sms_body, $sms_template->template_id)) :
                    return response()->json([
                        'error' => __('Unable to send otp')
                    ]);
                endif;
            endif;
            $request['otp'] = $otp;
            RegistrationRequest::create($request->all());

            DB::commit();
            return $this->responseWithSuccess(__('OTP Send Successfully'), [], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->responseWithError($e->getMessage(), [], 500);
        }
    }

    public function verifyRegistrationOTP(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $validator = Validator::make($request->all(), [
                'phone' => 'required',
                'otp'   => 'required'
            ]);

            if ($validator->fails()) {
                return $this->responseWithError(__('Required field missing'), $validator->errors(), 422);
            }

            $user = User::where('phone', $request->phone)
                ->where('status', 0)
                ->where('is_deleted', 0)
                ->first();
            if (!$user) {
                return $this->responseWithError(__('User Not Found'), [], 404);
            }


            $sid    = "AC073fdbc1ea233e274cd67250039c10be";
            $token  = "56c6e67090f38b7760a3f8d87e49e360";

            $twilio = new Client($sid, $token);


            $verification_check = $twilio->verify->v2->services("VAd9a582d2f53b1bd0f1983eee974a4752")
                ->verificationChecks
                ->create([
                    'to' => '+' . $request->phone,
                    'code' => $request->otp
                ]);
            // إذا كان التحقق ناجحًا
            if ($verification_check->status == "approved") {
                $token = JWTAuth::fromUser($user);
                $data['token'] = $token;
                $data['first_name'] = $user->first_name;
                $data['last_name'] = $user->last_name;
                $data['id'] = $user->id;
                $data['image'] = $user->profile_image;
                $data['phone'] = nullCheck($user->phone);
                $data['wp_number'] = nullCheck($user->wp_number);
                $data['email'] = nullCheck($user->email);
                $data['own_code'] = nullCheck($user->own_code);
                $data['invitation_code'] = nullCheck($user->invitation_code);
                $data['socials'] = $user->socials == null ? [] : $user->socials;

                $user->status = 1;
                $user->save();
                return $this->responseWithSuccess(__('Registration Successfully'), $data, 200);
            } else {
                return $this->responseWithError('Error OTP', [], 404);
            }

            // $userOtp = $user->otp;
            // $otp = $request->otp;

            // if($otp != $userOtp){
            //     return $this->responseWithError(__('OTP Is Wrong'),[], 404);
            // }

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->responseWithError($e->getMessage(), [], 500);
        }
    }

    public function logout(Request $request)
    {
        try {
            Sentinel::logout();
            JWTAuth::invalidate(JWTAuth::getToken());
            return $this->responseWithSuccess(__('Logout Successfully'), [], 200);
        } catch (JWTException $e) {
            JWTAuth::unsetToken();
            return $this->responseWithError(__('Failed to logout'), [], 500);
        }
    }

    public function getOtp(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            if (settingHelper('disable_otp_verification') == 1) :
                return $this->responseWithError(__('OTP Verification is Disabled'), [], 401);
            endif;

            $validator = Validator::make($request->all(), [
                'phone' => 'required|max:255',
            ]);

            if ($validator->fails()) {
                return $this->responseWithError(__('Required field missing'), $validator->errors(), 422);
            }

            // if ($user->status == 0) :
            //     return $this->responseWithError(__('Your account status is inactive'), [], 500);

            // elseif ($user->status == 2) :
            //     return $this->responseWithError(__('Your account is suspend'), [], 500);

            // elseif (!Activation::completed($user)) :
            //     return $this->responseWithError(__('Your account is not verified.Please verify your account.'), [], 500);
            // endif;

            // $sms_templates = AppSettingUtility::smsTemplates();

            // $sms_template = $sms_templates->where('tab_key', 'login')->first();
            // $otp = rand(10000, 99999);
            // $sms_body = str_replace('{otp}', $otp, $sms_templ    ate->sms_body);
            if (addon_is_activated('otp_system')) :
                $this->send($request->phone, '',);
                return $this->responseWithSuccess(__('OTP Sended'), [], 200);

            else :
                return $this->responseWithSuccess(__('Service is unavailable'), [], 400);
            endif;
        } catch (\Exception $e) {
            return $this->responseWithError(__($e->getMessage()), [], 500);
        }
    }

    public function verifyLoginOtp(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            if (settingHelper('disable_otp_verification') == 1) {
                $validator = Validator::make($request->all(), [
                    'phone' => 'required|max:255'
                ]);
            } else {
                $validator = Validator::make($request->all(), [
                    'phone' => 'required|max:255',
                    'otp' => 'required'
                ]);
            }

            $request['phone'] = str_replace(' ', '', $request->phone);

            $user = User::where('phone', $request->phone)
                ->where('status', 0)
                ->where('is_deleted', 0)
                ->first();

            if (blank($user)) {
                return $this->responseWithError(__('User not found'), [], 422);
            }
            if ($user->is_user_banned == 1) {
                return $this->responseWithError(__('Your account has been banned'), [], 401);
            }
            if ($user->status == 2) :
                return $this->responseWithError(__('Your account is suspend'), [], 401);
            endif;

            $sid    = "AC073fdbc1ea233e274cd67250039c10be";
            $token  = "56c6e67090f38b7760a3f8d87e49e360";

            $twilio = new Client($sid, $token);

            // تحقق من الكود المدخل من قبل المستخدم
            $verification_check = $twilio->verify->v2->services("VAd9a582d2f53b1bd0f1983eee974a4752")
                ->verificationChecks
                ->create([
                    'to' => '+' . $request->phone,
                    'code' => $request->otp
                ]);

            // إذا كان التحقق ناجحًا
            if ($verification_check->status == "approved") {
                try {
                    if (!$token = JWTAuth::fromUser($user)) {
                        return $this->responseWithError(__('Invalid credentials'), [], 401);
                    }
                } catch (JWTException $e) {
                    return $this->responseWithError(__('Unable to login, please try again'), [], 422);
                } catch (ThrottlingException $e) {
                    return $this->responseWithError(__('Suspicious activity on your ip, try after') . ' ' . $e->getDelay() . ' ' . __('seconds'), [], 422);
                } catch (\Exception $e) {
                    return $this->responseWithError(__($e->getMessage()), [], 500);
                }

                $token = JWTAuth::fromUser($user);
                $data['token'] = $token;
                $data['first_name'] = $user->first_name;
                $data['last_name'] = $user->last_name;
                $data['id'] = $user->id;
                $data['image'] = $user->profile_image;
                $data['phone'] = nullCheck($user->phone);
                $data['wp_number'] = nullCheck($user->wp_number);
                $data['email'] = nullCheck($user->email);
                $data['own_code'] = nullCheck($user->own_code);
                $data['invitation_code'] = nullCheck($user->invitation_code);
                $data['socials'] = $user->socials == null ? [] : $user->socials;

                $user->status = 1;
                $user->save();

                return $this->responseWithSuccess(__('Login Successfully'), $data, 200);
            } else {
                return $this->responseWithError('Error OTP', [], 404);
            }
        } catch (\Exception $e) {
            return $this->responseWithError(__($e->getMessage()), [], 500);
        }
    }

    public function forgotPassword(Request $request): \Illuminate\Http\JsonResponse
    {
        // return response()->json();
        $validator = Validator::make($request->all(), [
            'phone' => 'required|exists:users'
        ]);
        if ($validator->fails()) {

            return $this->responseWithError($validator->errors(), [], 422);
        }
        try {
            $user = User::where('phone', $request->phone)->first();

            if ($user) {
                if ($user->status == 0) :
                    return $this->responseWithError(__('Your account status is inactive'), []);
                elseif ($user->status == 2) :
                    return $this->responseWithError(__('Your account is suspend'), []);
                elseif ($user->status == 3) :
                    return $this->responseWithError(__('Your account has been banned'), []);
                endif;

                // $otp = rand(1000, 9999);
                // PasswordRequest::where('user_id', $user->id)->delete();

                // PasswordRequest::create([
                //     'user_id' => $user->id,
                //     'otp' => $otp,
                // ]);

                // $reminder = Reminder::create($user);
                //                sendMail($user, $reminder->code, 'forgot_password', '','',$otp);
                // $this->sendmail($request->email, 'Forgot Password', $user, 'email.auth.forgot-password-email', url('/') . '/activation/' . $request->email . '/' . $reminder->code, '', $otp);
                // $this->send($user->wp_number,"Mally: ".$otp);
                $this->sendOTP($request);

                return $this->responseWithSuccess(__('You have received an email for reset your password'), ['code' => '0000']);
            } else {
                return $this->responseWithError(__('User not found'), [], 422);
            }
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), [], 500);
        }
    }

    public function createNewPassword(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6|max:32|confirmed'
        ]);

        if ($validator->fails()) {
            return $this->responseWithError(__('Required field missing'), $validator->errors(), 422);
        }

        try {
            $user = User::where('phone', $request->phone)->first();
            if ($user) {
                $user->password = bcrypt($request->password);
                $user->save();
                return $this->responseWithSuccess(__('Successfully Password Changed'), [], 200);
            } else {
                return $this->responseWithError(__('User Not Foune'), [], 404);
            }

            // $user = User::where('wp_number', $request->wp_number)->first();
            // $otp = PasswordRequest::where('otp', $request->otp)->where('user_id', $user->id)->latest()->first();

            // $reminder = Reminder::exists($user, $request->code);
            // if ($otp && $reminder) {
            //     $user->password = bcrypt($request->password);
            //     $otp->delete();

            //     Reminder::complete($user, $request->code, $request->password);
            //     //                sendMail($user, '', 'reset_password', $request->password);
            //     // $this->sendmail($request->email, 'Forgot Password', $user, 'email.auth.forgot-password-email', url('/') . '/reset/' . $request->email . '/' . $reminder->code);

            // return $this->responseWithSuccess(__('Successfully Password Changed'), [], 200);
            // } else {
            // return $this->responseWithError(__('Please Request Another Code'), [], 500);
            // }
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), [], 500);
        }
    }

    public function socialLogin(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'uid' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->responseWithError(__('Required field missing'), $validator->errors(), 422);
            }


            $user = User::where('firebase_auth_id', $request->uid)->where('user_type', 'customer')->first();

            if ($user) :
                if ($user->is_user_banned == 1) :
                    return $this->responseWithError(__('Your account is banned'), []);
                elseif ($user->status == 0) :
                    return $this->responseWithError(__('Your account is inactive'), []);
                endif;
            else :
                $images = [];

                try {
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $request->image,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                    ));

                    $response = curl_exec($curl);

                    $err = curl_error($curl);
                    curl_close($curl);
                    $url = $response;
                } catch (\Exception $e) {
                    $url = '';
                }


                if ($url) {
                    $images = $this->saveImage('', '_staff_', '', $url);
                } else if ($request->image) {
                    $images = $this->saveImage('', '_staff_', '', file_get_contents($request->image));
                }

                $name = explode(' ', $request->name);
                $credentials = [
                    'first_name' => array_key_exists(0, $name) ? $name[0] : '',
                    'last_name' => array_key_exists(1, $name) ? $name[1] : ' ' . (array_key_exists(2, $name) ? ' ' . $name[2] : ''),
                    'email' => $request->email ?: '',
                    'phone' => $request->phone ?: '',
                    'images' => array_key_exists('images', $images) ? $images['images'] : [],
                    'password' => 'social-login',
                    'user_type' => 'customer',
                    'date_of_birth' => $request->dob ? Carbon::parse($request->dob)->format('Y-m-d') : null,
                    'gender' => $request->gender,
                    'firebase_auth_id' => $request->uid,
                    'permissions' => [],
                    'is_password_set' => 0,
                ];

                $user = Sentinel::register($credentials);
                $activation = Activation::create($user);
                Activation::complete($user, $activation->code);
            endif;

            try {
                Sentinel::authenticate($user);
            } catch (NotActivatedException $e) {
                return $this->responseWithError(__('Your account is not verified.Please verify your account.'), [], 500);
            }

            try {
                if (!$token = JWTAuth::fromUser($user)) :
                    return $this->responseWithError(__('Invalid credentials'), [], 401);
                endif;
            } catch (JWTException $e) {
                return $this->responseWithError(__('Unable to login, please try again'), [], 422);
            } catch (ThrottlingException $e) {
                return $this->responseWithError(__('Suspicious activity on your ip, try after') . ' ' . $e->getDelay() . ' ' . __('seconds'), [], 422);
            } catch (\Exception $e) {
                return $this->responseWithError(__($e->getMessage()), [], 500);
            }
            $data['token'] = $token;
            $data['first_name'] = $user->first_name;
            $data['last_name'] = $user->last_name;
            $data['image'] = $user->profile_image;
            $data['phone'] = nullCheck($user->phone);
            $data['email'] = nullCheck($user->email);
            $data['socials'] = $user->socials;
            Cart::where('user_id', getWalkInCustomer()->id)->where('trx_id', $request->trx_id)->update(['user_id' => $user->id]);

            return $this->responseWithSuccess(__('Login Successfully'), $data, 200);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), [], 500);
        }
    }

    public function verifyOtp(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }
        try {

            $user = User::where('phone', $request->phone)->first();
            // $request = PasswordRequest::where('otp', $request->otp)->where('user_id', $user->id)->latest()->first();
            $sid    = "AC073fdbc1ea233e274cd67250039c10be";
            $token  = "56c6e67090f38b7760a3f8d87e49e360";

            $twilio = new Client($sid, $token);

            $verification_check = $twilio->verify->v2->services("VAd9a582d2f53b1bd0f1983eee974a4752")
                ->verificationChecks
                ->create([
                    'to' => '+' . $request->phone,
                    'code' => $request->otp
                ]);
            if ($verification_check->status == "approved") {
                return $this->responseWithSuccess(__('otp_verified'), []);
            } else {
                return $this->responseWithError(__('request_another_one'), [], 403);
            }
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), [], 500);
        }
    }

    public function createPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }
        try {
            $user = User::where('phone', $request->phone)->where('status', 1)->first();
            // $otp = PasswordRequest::where('otp', $request->otp)->where('user_id', $user->id)->where('status', 0)->latest()->first();
            if ($otp) {
                $user->password = bcrypt($request->password);
                $user->save();
                $otp->delete();
                return response()->json([
                    'success' => "password hs been changed",
                    'changed' => true,
                ], 200);
                //change password
            } else {
                return response()->json(['errors' => "Please Request Another One"], 403);
            }
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()]);
        }
    }

    public function activeOrDeactiveNotification(Request $request)
    {
        try {
            $user = null;
            if ($request->token) {
                try {
                    if (!$user = JWTAuth::parseToken()->authenticate()) {
                        return $this->responseWithError(__('unauthorized_user'), [], 401);
                    }
                } catch (\Exception $e) {
                }
            }
            $user->is_notify = $request->is_notify == 'true';
            $user->save();
            return response()->json(['message' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }
    }
}
