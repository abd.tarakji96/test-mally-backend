<?php

namespace App\Http\Controllers\Api\V100;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\OrderPaginateResource;
use App\Http\Resources\Api\PeriodicOrderPaginateResource;
use App\Models\Order;
use App\Repositories\Interfaces\Admin\OrderInterface;
use App\Repositories\Interfaces\Site\CartInterface;
use App\Traits\ApiReturnFormatTrait;
use App\Traits\PaymentTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class PeriodicOrderController extends Controller
{
    use ApiReturnFormatTrait,PaymentTrait;
    protected $order;

    public function __construct(OrderInterface $order)
    {
        $this->order = $order;
    }
    
    public function index(OrderInterface $orderList,Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $user = null;
            if ($request->token)
            {
                try {
                    if (!$user = JWTAuth::parseToken()->authenticate()) {
                        return $this->responseWithError(__('unauthorized_user'), [], 401);
                    }
                } catch (\Exception $e) {
                    return $this->responseWithError(__('unauthorized_user'), [], 401);
                }
            }
            
            $q = Order::withoutGlobalScope('normal_order');
            $q->where('order_type', 'periodic');
            $q->where('user_id', $user->id);//->orWhere('seller_id', $user->id);
            $orders = $q->paginate(get_pagination('api_paginate'));

            $data = [
                'orders' => PeriodicOrderPaginateResource::collection($orders),
            ];
            return $this->responseWithSuccess(__('Orders Found Successfully'), $data, 200);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), [], null);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,CartInterface $cart): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();
        try {
            $user= null;
            if ($request->token)
            {
                try {
                    if (!$user = JWTAuth::parseToken()->authenticate()) {
                        return $this->responseWithError(__('unauthorized_user'), [], 401);
                    }
                } catch (\Exception $e) {
                }
            }

            if (!$user && settingHelper('disable_guest_checkout') == 1) {
                return $this->responseWithError(__('unauthorized_user'), [], 401);
            }

            
            $carts = $cart->cartList($user,['trx_id' => $request->trx_id]);
            $checkout = $cart->checkoutCoupon($carts, ['coupon'],$user);
            
            $data= $request->all();
            $data['order_type']= 'periodic';

            $order = $this->order->confirmOrder($checkout, $carts, $data,$user);

            DB::commit();

            if (is_array($order)) {
                return $this->responseWithSuccess(__('Order done'), $order, 200);
            } else {
                $data = [
                    'error' => $order
                ];
                return $this->responseWithError(__('Something Went Wrong'), $data, 500);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->responseWithError($e->getMessage(), [], null);
        }
    }

    public function show(Order $order)
    {
        return response()->json($order, 200);
    }

    public function update(Request $request, Order $order)
    {
       try{
        $order = Order::find($request->id);

        $freq = $request->frequency;
        if(isset($request->is_active)){
            $order->is_active = $request->is_active;
        }else if($freq == 'none'){
            $order->order_type = 'normal';
        }else if($freq == 'daily'){
            $order->frequency = $freq; 
        }else{
            $order->frequency = $freq;
            $order->days = $request->days; 
        }

        $order->save();

        return $this->responseWithSuccess(__('تم التعديل بنجاح'), $order, 200);

       }catch(\Exception $e){
            return $this->responseWithError($e->getMessage(), [], null);
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return response()->json(null, 204);
    }
}
