<?php

namespace App\Http\Controllers\Api\V100;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\RecipeResource;
use App\Http\Resources\RecipeCategoryResource;
use App\Models\RecipeCategory;
use App\Repositories\Interfaces\Admin\Product\CategoryInterface;
use App\Repositories\Interfaces\Admin\Recipe\RecipeCategoryInterface;
use App\Repositories\Interfaces\Admin\Recipe\RecipeInterface;
use App\Repositories\Interfaces\UserInterface;
use App\Traits\ApiReturnFormatTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecipeController extends Controller
{
    use ApiReturnFormatTrait;

    public $recipe;
    public $category;
    public $user;

    public function __construct(RecipeInterface $recipe,CategoryInterface $category, UserInterface $user)
    {
        $this->recipe   = $recipe;
        $this->category = $category;
        $this->user     = $user;
    }

    public function allRecipe()
    {
        try {
            $categories = RecipeCategory::latest()->with('currentLanguage')->where('status',1)->get();

            $data = [
                'categories' => RecipeCategoryResource::collection($categories),
                'recipes'    => RecipeResource::collection($this->recipe->allRecipe(get_pagination('api_paginate'))),
            ];
            
            return $this->responseWithSuccess(__('Data Successfully Found'), $data, 200);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), [], null);
        }
    }
    public function recipeById($id)
    {
        try {
            $recipeDetails = $this->recipe->get($id);
            $data =[
                'id'                => $recipeDetails->id,
                'category_id'       => $recipeDetails->category_id,
                'category_title'    => $this->category->get($recipeDetails->category_id)->title,
                'user_id'           => $recipeDetails->user_id,
                'user_name'         => $this->user->get($recipeDetails->user_id)->fullname,
                'image'             => [
                    'image_40x40'       => getFileLink('40x40', $recipeDetails->image),
                    'image_72x72'       => getFileLink('72x72', $recipeDetails->image),
                    'image_190x230'     => getFileLink('190x230', $recipeDetails->image),
                    'image_110x122'     => getFileLink('110x122', $recipeDetails->image),
                    'image_320x320'     => getFileLink('320x320', $recipeDetails->image),
                    'image_260x175'     => getFileLink('260x175', $recipeDetails->image),
                ],
                'banner'            => [
                    'image_40x40'       => getFileLink('40x40', $recipeDetails->image),
                    'image_72x72'       => getFileLink('72x72', $recipeDetails->image),
                    'image_190x230'     => getFileLink('190x230', $recipeDetails->image),
                    'image_110x122'     => getFileLink('110x122', $recipeDetails->image),
                    'image_320x320'     => getFileLink('320x320', $recipeDetails->image),
                    'image_900x300'     => getFileLink('900x300', $recipeDetails->image),
                ],
                'status'            => $recipeDetails->status,
                'title'             => $recipeDetails->title,
                'short_description' => $recipeDetails->short_description,
                'description'       => $recipeDetails->description,
                'long_description' => $recipeDetails->long_description,

                'tags'              => $recipeDetails->tags,
                'published_date'    => $recipeDetails->published_date,
            ];

            return $this->responseWithSuccess(__('Data Successfully Found'), $data, 200);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), [], null);
        }
    }

    public function details($id, RecipeCategoryInterface $recipeCategory): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $recipe = $this->recipe->recipeById($id);

            $categories = $recipeCategory->all()->with('currentLanguage')->where('status',1)->get();

            $data = [
                'categories' => RecipeCategoryResource::collection($categories),
                'recipe' => [
                    'id'                => $recipe->id,
                    'banner'            => $recipe->banner_img,
                    'date'              => $recipe->published_date,
                    'title'             => $recipe->title,
                    'tags'              => explode(',', $recipe->tags),
                    'description'       => html_entity_decode($recipe->description),
                    'user'              => [
                        'id'        => $recipe->user->id,
                        'name'      => $recipe->user->full_name,
                        'image'     => $recipe->user->user_profile_image,
                    ],
                ],
                'recent_posts' => RecipeResource::collection($this->recipe->recentPosts($recipe->id)),
            ];
            DB::commit();
            return $this->responseWithSuccess(__('Data Successfully Found'), $data, 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->responseWithError($e->getMessage(), [], null);
        }
    }

    public function getDetails(Request $request, $id)
    {
        try {
            $recipe = $this->recipe->get($id);
            $data = [
                'details' => $recipe->getTranslation('long_description',apiLanguage($request->lang))
            ];
            return view('api.post-details',$data);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), [], null);
        }
    }

    public function search(Request $request){
        try {
            $categories = RecipeCategory::latest()->with('currentLanguage')->where('status',1)->get();

            $data = [
                'categories' => RecipeCategoryResource::collection($categories),
                'recipes'    => RecipeResource::collection($this->recipe->search($request->search,get_pagination('api_paginate'))),
            ];
            return $this->responseWithSuccess(__('Data Successfully Found'), $data, 200);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), [], null);
        }
    }
    public function filterCategory(Request $request){
        try {
            $categories = RecipeCategory::latest()->with('currentLanguage')->where('status',1)->get();

            $data = [
                'categories' => RecipeCategoryResource::collection($categories),
                'recipes'    => RecipeResource::collection($this->recipe->filterCategory($request->category_id,get_pagination('api_paginate'))),
            ];
            return $this->responseWithSuccess(__('Data Successfully Found'), $data, 200);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), [], null);
        }
    }
    
}
