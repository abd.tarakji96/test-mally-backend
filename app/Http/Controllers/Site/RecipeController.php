<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Resources\SiteResource\RecipePaginateResource;
use App\Repositories\Interfaces\Admin\Recipe\RecipeCategoryInterface;
use App\Repositories\Interfaces\Admin\Recipe\RecipeInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecipeController extends Controller
{
    protected $recipe;

    public function __construct(RecipeInterface $recipe)
    {
        $this->recipe = $recipe;
    }

    public function recipes(Request $request): \Illuminate\Http\JsonResponse
    {

        try {
            $data = [
                'recipe' => new RecipePaginateResource($this->recipe->recipes($request->all())),
            ];
            return response()->json($data);
        } catch (\Exception $e) {
            return response()->json([
                'error' =>  $e->getMessage()
            ]);
        }
    }

    public function recipeDetails($slug, RecipeCategoryInterface $recipeCategory): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $recipe = $this->recipe->recipeDetails($slug);
            $recipe->title = $recipe->getTranslation('title',languageCheck());
            $recipe->short_description = $recipe->getTranslation('short_description',languageCheck());
            $recipe->long_description = $recipe->getTranslation('long_description',languageCheck());
            $data = [
                // 'recipe_view' => $this->recipe->recipeView($recipe),
                // 'comments' => $this->recipe->comments($recipe->id, 10),
                'categories' => $recipeCategory->paginate(14),
                'recipe' => $recipe,
                'recipe_ingredients' => $recipe->products,
                'tags' => explode(',', $recipe->tags),
                'recent_posts' => $this->recipe->recentPosts($recipe->id),
            ];
            DB::commit();
            return response()->json($data);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'error' =>  $e->getMessage()
            ]);
        }
    }
}
