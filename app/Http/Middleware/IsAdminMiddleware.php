<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Sentinel;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class IsAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // الحصول على اللغة الحالية
        $currentLocale = LaravelLocalization::getCurrentLocale();

        // طباعة اللغة الحالية
        // dd($currentLocale); 
        if (Sentinel::check()):
            if (Sentinel::getUser()->user_type == 'admin' || Sentinel::getUser()->user_type == 'staff'):
                return $next($request);
            elseif(Sentinel::getUser()->user_type == 'seller'):
                return redirect()->route('home');
            else:
                return redirect()->route('home');
            endif;
        else:
            return  redirect()->route('admin.login.form');
        endif;
    }
}
