<?php

namespace App\Http\Requests\Admin\Recipe;

use Illuminate\Foundation\Http\FormRequest;

class RecipeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'                     =>'required|max:300',
            'slug'                      =>'max:70|unique:blogs,slug,'.\Request()->blog_id,
            'category_id'               =>'required',
            'short_description'         =>'nullable',
            'long_description'          =>'nullable',
            'image'                     =>'nullable',
            'meta_keyword'              =>'nullable',
            'status'                    =>'required',
            'meta_title'                =>'nullable',
            'meta_description'          =>'nullable',
            'products_ids'              =>'array',
            'products_ids.*'            =>'integer|exists:products,id',
            'products_quantity'         =>'array',
        ];
    }
}
