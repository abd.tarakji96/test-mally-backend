<?php

namespace App\Http\Resources\Api;

use App\Http\Resources\RecipeProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class RecipeResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'slug'                  => $this->slug,
            'url'                   => route('api.post.details',$this->id),
            'title'                 => $this->getTranslation('title',apiLanguage($request->lang)),
            'short_description'     => nullCheck($this->getTranslation('short_description',apiLanguage($request->lang))),
            'long_description'      => nullCheck($this->getTranslation('long_description',apiLanguage($request->lang))),
            'thumbnail'             => $this->thumbnail,
            'recipe_products'       => RecipeProductResource::collection($this->recipe_products),
            'category_id'           => $this->category_id,
        ];
    }
}
