<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class StockResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'variant_ids'           => $this->variant_ids,
            'product_id'            => $this->product_id,
            'name'                  => $this->name,
            'sku'                   => $this->sku,
            'current_stock'         => $this->current_stock,
            'price'                 => $this->price,
            'image'                 => $this->image,
            'image_id'              => $this->image_id,
        ];
    }
}
