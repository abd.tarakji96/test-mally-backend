<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShippingCostsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'city_id'                    => $this->id,
            'cost'                       => $this->cost,
        ];
    }
}
