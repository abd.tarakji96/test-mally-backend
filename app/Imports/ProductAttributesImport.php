<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\ProductStock;
use App\Traits\SlugTrait;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Sentinel;

class ProductAttributesImport implements ToCollection, WithHeadingRow, WithChunkReading, SkipsEmptyRows, SkipsOnError, WithValidation
{
    use SlugTrait, SkipsErrors, Importable;
    
    public function collection(Collection $rows)
    {
        if (Sentinel::getUser()->user_type == 'seller'):
            $user_id = authId();
        else:
            $user_id = 1;
        endif;
        //Edit Products

        foreach ($rows as $row):
            $productAttribute = ProductStock::firstOrNew(['id' => $row['id']]);
        
            $productAttribute->price = $row['price'];
            $productAttribute->current_stock = $row['current_stock'];
            $productAttribute->product->price = $row['price'];
            $productAttribute->product->current_stock = $row['current_stock'];
            $productAttribute->product->productLanguages[0]->short_description = $row['short_description'];
            $productAttribute->product->productLanguages[0]->description = $row['description'];
            
            $productAttribute->product->productLanguages[0]->save();
            $productAttribute->product->save();

            $productAttribute->save();
        endforeach;
    }

    public function chunkSize(): int
    {
        return 2000;
    }

    public function rules(): array
    {
        return [
        ];
    }
}
