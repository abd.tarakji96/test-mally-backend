<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Order;
use App\Models\User;
use App\Models\ScheduledOrder;
use Carbon\Carbon;
use app\Helpers\Helpers;
class CheckRemindingTime implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $scheduledOrder;
    protected $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ScheduledOrder $scheduledOrder,  User $user)
    {
        $this->scheduledOrder = $scheduledOrder;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $message = "Your order will be shipped in $this->scheduledOrder->datetime";
        sendNotification($this->user, $message , 'success', null, null);
    }
}
