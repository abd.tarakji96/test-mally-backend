<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Order;
use App\Models\ScheduledOrder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CheckShippingTime implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $scheduledOrder;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ScheduledOrder $scheduledOrder)
    {
        $this->scheduledOrder = $scheduledOrder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        if($this->scheduledOrder->status != 'canceled')
        {
            // normal order instance
            $orderId = $this->scheduledOrder->order_id;
            $result  = DB::select("SELECT * FROM orders WHERE id = $orderId");
            $periodicOrder = reset($result);
            $normalOrder = new Order();
            $normalOrder->order_type = 'normal';
            $normalOrder->frequency = null;
            $normalOrder->days = null;
            $normalOrder->time = null;
            $normalOrder->remind_before = null;
            $normalOrder->urgent = 0;
            $orderDetails = [];
            foreach ($periodicOrder->orderDetails as $orderDetail) {
                $newOrder = $orderDetail;
                $newOrder->order_id = $normalOrder->id;
                $orderDetails[] = $newOrder; 
            }
            $normalOrder->orderDetails()->saveMany($orderDetails);

            $normalOrder->save();
            $this->scheduledOrder->status = 'confirmed';
            $this->scheduledOrder->save();
        }
    }
}
