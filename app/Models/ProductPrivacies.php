<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductPrivacies extends Model
{
    use HasFactory;

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_privacy_products');
    }

    public function privaciesLanguages()
    {
        return $this->hasMany(ProductPrivacyLanguage::class,'product_privacy_id');
    }

    public function getTranslation($field, $lang = 'en')
    {
//        $lang = \App::getLocale();
        $product_privacy_translation  = $this->hasMany(ProductPrivacyLanguage::class,'product_privacy_id')->where('lang', $lang)->first();
        if (blank($product_privacy_translation)):
            $product_privacy_translation = $this->hasMany(ProductPrivacyLanguage::class,'product_privacy_id')->where('lang', 'en')->first();
        endif;

        return $product_privacy_translation->$field;
    }

    public function currentLanguage(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ProductPrivacyLanguage::class);
    }

    public function getTranslateAttribute()
    {
        $lang = languageCheck();

        $row = $this->currentLanguage->where('product_privacy_id',$this->id)->where('lang',$lang)->first();
        if (!$row)
            $row = $this->currentLanguage->where('product_privacy_id',$this->id)->where('lang','en')->first();

        return $row;
    }

    public function getNameAttribute()
    {
        return @$this->translate->name;
    }
    
    
}
