<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductPrivacyLanguage extends Model
{
    use HasFactory;

    public function productPrivacy()
    {
        return $this->belongsTo(ProductPrivacies::class,'product_privacy_id');
    }

}