<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecipeCategoryLanguage extends Model
{
    use HasFactory;
    public function recipeCategory()
    {
        return $this->belongsTo(RecipeCategory::class);
    }
}
