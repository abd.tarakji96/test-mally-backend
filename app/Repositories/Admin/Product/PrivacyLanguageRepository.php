<?php

namespace App\Repositories\Admin\Product;

use App\Models\ProductPrivacyLanguage;
use App\Repositories\Interfaces\Admin\Product\PrivacyLanguageInterface;
use App\Traits\SlugTrait;
use Illuminate\Support\Facades\DB;

class PrivacyLanguageRepository implements PrivacyLanguageInterface
{
    use SlugTrait;

    public function get($id)
    {
        return ProductPrivacyLanguage::find($id);
    }


    public function all()
    {
        return ProductPrivacyLanguage::latest();
    }
    public function getByLang($id, $request)
    {
        return ProductPrivacyLanguage::where('product_privacy_id', $id)->where('lang', $request->lang);
    }

    public function paginate($limit)
    {
        return $this->all()->paginate($limit);
    }

    public function store($request)
    {
        DB::beginTransaction();
        try {
            $productPrivacyLanguage                      = new  ProductPrivacyLanguage();
            $productPrivacyLanguage->privacy_name                = $request->privacy_name;
            $productPrivacyLanguage->product_privacy_id            = $request->product_privacy_id;
            $productPrivacyLanguage->lang                = $request->lang;
            $productPrivacyLanguage->save();
            DB::commit();
            return true;
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            return false;
        }
    }

    public function update($request)
    {
        DB::beginTransaction();
        try {
            $productPrivacyLanguage                      = $this->get($request->product_privacy_lang_id);
            $productPrivacyLanguage->privacy_name               = $request->privacy_name;
            $productPrivacyLanguage->product_privacy_id            = $request->product_privacy_id;
            $productPrivacyLanguage->lang                = $request->lang != '' ? $request->lang : 'en';

            $productPrivacyLanguage->save();

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

}