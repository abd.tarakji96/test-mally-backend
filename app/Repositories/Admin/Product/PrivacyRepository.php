<?php

namespace App\Repositories\Admin\Product;

use App\Models\ProductPrivacies;
use App\Models\ProductPrivacyLanguage;
use App\Repositories\Interfaces\Admin\Product\PrivacyInterface;
use App\Repositories\Interfaces\Admin\Product\PrivacyLanguageInterface;

class PrivacyRepository implements PrivacyInterface
{

    protected $privacyLanguage;

    public function __construct(PrivacyLanguageInterface $privacyLanguage)
    {
        $this->privacyLanguage = $privacyLanguage;
    }

    public function getByLang($id, $lang)
    {
        if ($lang == null):
           $privacyByLang = ProductPrivacyLanguage::with('productPrivacy')
                ->where('lang', 'en')
                ->where('product_privacy_id', $id)
                ->first();
        else:
            $privacyByLang = ProductPrivacyLanguage::with('productPrivacy')
                ->where('lang', $lang)
                ->where('product_privacy_id', $id)
                ->first();
            if (blank($privacyByLang)):
                $privacyByLang = ProductPrivacyLanguage::with('productPrivacy')
                    ->where('lang', 'en')
                    ->where('product_privacy_id', $id)
                    ->first();
                $privacyByLang['translation_null'] = 'not-found';
            endif;
        endif;
        return $privacyByLang;
    }

    public function get($id)
    {
        return ProductPrivacies::find($id);
    }

    public function store($request)
    {
        $productPrivacy               = new ProductPrivacies();
        $productPrivacy->privacy_name         = $request->privacy_name;
        $productPrivacy->save();

        $request['product_privacy_id'] = $productPrivacy->id;

        if ($request->lang == ''):
            $request['lang'] = 'en';
        endif;
        $this->privacyLanguage->store($request);
        return true;
    }

    public function update($request)
    {
        $productPrivacy             = $this->get($request->product_privacy_id);
        $productPrivacy->privacy_name        = $request->privacy_name;
        $productPrivacy->save();

        if ($request->product_privacy_lang_id == '') :
            $this->privacyLanguage->store($request);

        else:
            $this->privacyLanguage->update($request);
        endif;
        return true;
    }

    public function all()
    {
       return ProductPrivacies::leftJoin('product_privacy_languages', 'product_privacy_languages.product_privacy_id', '=', 'product_privacies.id')
            ->select('product_privacies.*', 'product_privacy_languages.id as product_privacy_lang_id', 'product_privacy_languages.privacy_name', 'product_privacy_languages.lang');    }

    public function paginate($limit)
    {
//        dd($this->all()->get());
        return $this->all()
        ->where('lang', 'en')
        ->orderBy('created_at', 'DESC')
        ->paginate($limit);
    }

    public function shopPrivacies()
    {
        return ProductPrivacies::latest()->paginate(8);
    }

    public function productPrivacyByIds($ids)
    {
        return ProductPrivacies::whereIn('id',$ids)->get();
    }
}