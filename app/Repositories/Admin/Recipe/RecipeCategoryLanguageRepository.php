<?php

namespace App\Repositories\Admin\Recipe;

use App\Models\RecipeCategoryLanguage;
use App\Repositories\Interfaces\Admin\Recipe\RecipeCategoryLanguageInterface;
use App\Traits\SlugTrait;
use DB;

class RecipeCategoryLanguageRepository implements RecipeCategoryLanguageInterface
{
    use SlugTrait;

    public function get($id)
    {
        return RecipeCategoryLanguage::find($id);
    }

    public function all()
    {
        return RecipeCategoryLanguage::latest();
    }

    public function paginate($limit)
    {
        return $this->all()->paginate($limit);
    }

    public function store($request)
    {
        DB::beginTransaction();
        try {
            $cateLang = new RecipeCategoryLanguage();

            $cateLang->title             = $request->title;
            $cateLang->recipe_category_id  = $request->recipe_category_id;
            $cateLang->lang              = $request->lang != '' ? $request->lang : 'en' ;
            $cateLang->meta_title        = $request->meta_title;
            $cateLang->meta_description  = $request->meta_description;
            $cateLang->save();

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function update($request)
    {

        DB::beginTransaction();
        try {
            $cateLang                    = $this->get($request->recipe_category_lang_id);

            $cateLang->title             = $request->title;
            $cateLang->recipe_category_id  = $request->recipe_category_id;
            $cateLang->lang              = $request->lang != '' ? $request->lang : 'en' ;
            $cateLang->meta_title        = $request->meta_title;
            $cateLang->meta_description  = $request->meta_description;
            $cateLang->save();

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

}

