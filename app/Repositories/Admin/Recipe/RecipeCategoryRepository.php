<?php

namespace App\Repositories\Admin\Recipe;

use App\Models\RecipeCategory;
use App\Models\RecipeCategoryLanguage;
use App\Repositories\Interfaces\Admin\Recipe\RecipeCategoryInterface;
use App\Repositories\Interfaces\Admin\Recipe\RecipeCategoryLanguageInterface;
use App\Traits\SlugTrait;
use DB;

class RecipeCategoryRepository implements RecipeCategoryInterface
{
    use SlugTrait;

    protected $recipeCategoryLang;

    public function __construct(RecipeCategoryLanguageInterface $recipeCategoryLang)
    {
        $this->recipeCategoryLang        = $recipeCategoryLang;
    }
    public function get($id)
    {
        return RecipeCategory::find($id);
    }

    public function all()
    {
        return RecipeCategory::latest();
    }

    public function paginate($limit)
    {
        return $this->all()->with('currentLanguage')->paginate($limit);
    }
    public function getByLang($id, $lang)
    {
        if($lang == null):
            $recipeCategoryByLang = RecipeCategoryLanguage::with('recipeCategory')->where('lang', 'en')->where('recipe_category_id', $id)->first();
        else:
            $recipeCategoryByLang = RecipeCategoryLanguage::with('recipeCategory')->where('lang', $lang)->where('recipe_category_id', $id)->first();
            if (blank($recipeCategoryByLang)):
                $recipeCategoryByLang = RecipeCategoryLanguage::with('recipeCategory')->where('lang', 'en')->where('recipe_category_id', $id)->first();
                $recipeCategoryByLang['translation_null'] = 'not-found';
            endif;
        endif;
        return $recipeCategoryByLang;
    }

    public function store($request)
    {
            $category                  = new  RecipeCategory();
            $category->slug            = $this->getSlug($request->title, $request->slug);
            $category->save();

            $request['recipe_category_id'] = $category->id;
            if ($request->lang == ''):
                $request['lang']    = 'en';
            endif;
            $this->recipeCategoryLang->store($request);
            return true;
    }

    public function update($request)
    {
            $category          = $this->get($request->recipe_category_id);
            $category->slug    = $this->getSlug($request->title, $request->slug);

            $category->save();

            if ($request->recipe_category_lang_id == '') :
                $this->recipeCategoryLang->store($request);
            else:
                $this->recipeCategoryLang->update($request);
            endif;
            return true;
    }

    public function statusChange($request)
    {
            $category           = $this->get($request['id']);
            $category->status   = $request['status'];
            $category->save();
            return true;
    }

}

