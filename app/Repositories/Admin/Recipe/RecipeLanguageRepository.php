<?php

namespace App\Repositories\Admin\Recipe;

use App\Models\RecipeLanguage;
use App\Repositories\Interfaces\Admin\Recipe\RecipeLanguageInterface;
use App\Traits\SlugTrait;
use DB;

class RecipeLanguageRepository implements RecipeLanguageInterface
{
    use SlugTrait;

    public function get($id)
    {
        return RecipeLanguage::find($id);
    }

    public function all()
    {
        return RecipeLanguage::latest();
    }

    public function paginate($limit)
    {
        return $this->all()->paginate($limit);
    }

    public function store($request)
    {
        DB::beginTransaction();
        try {
            $recipeLang                       = new RecipeLanguage();
            
            $recipeLang->title                = $request->title;
            $recipeLang->recipe_id            = $request->recipe_id;
            $recipeLang->lang                 = $request->lang != '' ? $request->lang : 'en' ;
            $recipeLang->short_description    = $request->short_description;
            $recipeLang->long_description     = $request->long_description;
            $recipeLang->meta_title           = $request->meta_title;
            $recipeLang->meta_description     = $request->meta_description;
            $recipeLang->meta_keyword         = $request->meta_keyword;
            $recipeLang->tags                 = $request->tags;

            $recipeLang->save();
            
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }

    }

    public function update($request)
    {

        DB::beginTransaction();
        try {
            $recipeLang                    = $this->get($request->recipe_lang_id);

            $recipeLang->title                = $request->title;
            $recipeLang->lang                 = $request->lang != '' ? $request->lang : 'en' ;
            $recipeLang->short_description    = $request->short_description;
            $recipeLang->long_description     = $request->long_description;
            $recipeLang->meta_title           = $request->meta_title;
            $recipeLang->meta_description     = $request->meta_description;
            $recipeLang->meta_keyword         = $request->meta_keyword;
            $recipeLang->tags                 = $request->tags;

            $recipeLang->save();

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

}

