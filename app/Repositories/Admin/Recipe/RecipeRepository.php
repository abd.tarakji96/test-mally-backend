<?php

namespace App\Repositories\Admin\Recipe;

use App\Models\Product;
use App\Models\Recipe;
use App\Models\RecipeLanguage;
use App\Repositories\Interfaces\Admin\Recipe\RecipeLanguageInterface;
use App\Repositories\Interfaces\Admin\Recipe\RecipeInterface;
use App\Traits\ImageTrait;
use App\Traits\SlugTrait;
use Illuminate\Support\Facades\DB;

class RecipeRepository implements RecipeInterface
{
    use SlugTrait;
    use ImageTrait;

    protected $recipeLang;

    public function __construct(RecipeLanguageInterface $recipeLang)
    {
        $this->recipeLang        = $recipeLang;
    }

    public function get($id)
    {
        return Recipe::find($id);
    }
    public function getByLang($id, $lang)
    {
        if($lang == null):
            $recipeByLang = RecipeLanguage::with('recipe')->where('lang', 'en')->where('recipe_id', $id)->first();
        else:
            $recipeByLang = RecipeLanguage::with('recipe')->where('lang', $lang)->where('recipe_id', $id)->first();
            if(blank($recipeByLang)):
                $recipeByLang = RecipeLanguage::with('recipe')->where('lang', 'en')->where('recipe_id', $id)->first();
                $recipeByLang['translation_null'] = 'not-found';
            endif;
        endif;

        return $recipeByLang;
    }

    public function all()
    {
        return Recipe::with('user','category')->latest();
    }

    public function paginate($request, $status, $limit)
    {
        return Recipe::with('user','category','recipeLanguages')
            ->when($status == 'trash' && $status != null, function ($query){
                $query->onlyTrashed();
            })
            ->when($status != 'trash' && $status != null, function ($query) use ($status){
                $query->where('status', $status);
            })
            ->when($request->c != null, function ($query) use ($request){
                $query->where('category_id', $request->c);
            })
            ->when($request->q != null, function ($query) use ($request){
                $query->whereHas('recipeLanguages', function ($q) use ($request){
                    $q->where('title', 'like', '%'.$request->q.'%');
                    $q->orwhere('short_description', 'like', '%'.$request->q.'%');
                    $q->orwhere('long_description', 'like', '%'.$request->q.'%');
                    $q->orwhere('meta_title', 'like', '%'.$request->q.'%');
                    $q->orwhere('meta_description', 'like', '%'.$request->q.'%');
                    $q->orwhere('meta_keyword', 'like', '%'.$request->q.'%');
                } );
                $query->orwhereHas('user', function ($q) use ($request){
                    $q->where('first_name', 'like', '%'.$request->q.'%');
                    $q->orwhere('last_name', 'like', '%'.$request->q.'%');
                    $q->orWhere(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%".$request->q."%");
                    $q->orwhere('email', 'like', '%'.$request->q.'%');
                });
            })
            ->latest()
            ->paginate($limit);

    }

    public function store($request)
    {

            $recipe = new Recipe();
            $recipe->category_id          = $request->category_id;
            $recipe->user_id              = authId();
            $recipe->slug                 =  $this->getSlug($request->title, $request->slug);

            if ($request->image != ''):
                $recipe->image            = $this->getImageWithRecommendedSize($request->image,260,175);
                $recipe->image_id         = $request->image;
            else:
                $recipe->image            = [];
                $recipe->image_id         = null;
            endif;

            if ($request->banner != ''):
                $recipe->banner            = $this->getImageWithRecommendedSize($request->banner,900,300);
                $recipe->banner_id         = $request->banner;
            else:
                $recipe->banner            = [];
                $recipe->banner_id         = null;
            endif;

            $recipe->status = $request->status;
            $recipe->save();

            $products_ids = $request->products_ids;
            $products_quantity = $request->products_quantity;

            if($products_ids){
                if(count($products_ids) != count($products_quantity))
                    return back()->withError(__('Something Wrong'));
                $_recipe_items = [];
                for($i=0; $i<count($products_ids); $i++){
                    $_recipe_items[$products_ids[$i]] = [
                        'quantity'     => $products_quantity[$i],
                    ];
                }
                $recipe->products()->sync($_recipe_items);
            }

            $request['recipe_id'] = $recipe->id;
            if ($request->lang == ''):
                $request['lang']    = 'en';
            endif;
            $this->recipeLang->store($request);
            return true;
    }

    public function update($request)
    {
            $recipe                       = $this->get($request->recipe_id);
            $recipe->category_id          = $request->category_id;
            $recipe->slug                 =  $this->getSlug($request->title, $request->slug);

            if ($request->image != ''):
                $recipe->image            = $this->getImageWithRecommendedSize($request->image,260,175);
                $recipe->image_id         = $request->image;
            else:
                $recipe->image            = [];
                $recipe->image_id         = null;
            endif;

            if ($request->banner != ''):
                $recipe->banner            = $this->getImageWithRecommendedSize($request->banner,900,300);
                $recipe->banner_id         = $request->banner;
            else:
                $recipe->banner            = [];
                $recipe->banner_id         = null;
            endif;

            $recipe->status = $request->status;
            $recipe->save();

            if ($request->recipe_lang_id == '') :
                $this->recipeLang->store($request);
            else:
                $this->recipeLang->update($request);
            endif;

            $products_ids = $request->products_ids;
            $products_quantity = $request->products_quantity;

            if($products_ids){
                if(count($products_ids) != count($products_quantity))
                    return back()->withError(__('Something Wrong'));
                $_recipe_items = [];
                for($i=0; $i<count($products_ids); $i++){
                    $_recipe_items[$products_ids[$i]] = [
                        'quantity'     => $products_quantity[$i],
                    ];
                }
                $recipe->products()->sync($_recipe_items);
            } else {
                $recipe->products()->sync([]);
            }
            
            return true;
    }
    public function restore($id)
    {
            $recipe = Recipe::withTrashed()->find($id);
            $recipe->status = 'published';
            $recipe->save();

            Recipe::withTrashed()->find($id)->restore();
            return true;
    }

    public function statusChange($request)
    {
//        DB::beginTransaction();
//        try {
//            $category           = $this->get($request['id']);
//            $category->status   = $request['status'];
//            $category->save();
//
//            DB::commit();
//            return true;
//        } catch (\Exception $e) {
//            DB::rollback();
//            return false;
//        }
    }

    //for api
    public function homePageRecipes()
    {
        return Recipe::with('currentLanguage')->where('status','published')->take(4)->latest()->select('id','image','image_id','slug')->get();
    }

    public function recipes($data)
    {
        $recipes = Recipe::query();

        $sort = array_key_exists('sort',$data) ? $data['sort'] : 'newest';

        if (array_key_exists('slug',$data))
        {
            $recipes->whereHas('category',function ($query) use ($data){
                $query->where('slug',$data['slug']);
            });
        }

        if (array_key_exists('title', $data) && !empty($data['title'])) {
            $recipes->whereHas('recipeLanguages', function ($query) use ($data) {
                $query->where('title', 'like', '%' . $data['title'] . '%');
            });
        }

        if ($sort == 'newest')
        {
            $recipes->latest();
        }
        if ($sort == 'oldest')
        {
            $recipes->oldest();
        }
        

        return $recipes->where('status','published')->paginate(12);
    }


    public function recipeDetails($slug)
    {
        return Recipe::with('user:id,images,first_name,last_name','currentLanguage')->where('slug',$slug)->first();
    }

    public function recipeById($id)
    {
        return Recipe::with('user:id,images,first_name,last_name','currentLanguage')->find($id);
    }

    public function comments($id,$paginate): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return RecipeComment::with('user:id,images,first_name,last_name','commentReplies.replies','commentLikes')
            ->where('recipe_id',$id)->latest()->paginate($paginate);
    }

    public function storeComment($request)
    {
        return RecipeComment::create($request);
    }

    public function storeReply($request)
    {
        if (empty($request['parent_id']))
        {
            $request['parent_id'] = null;
        }

        return RecipeCommentReply::create($request);
    }

    public function storeLike($data)
    {
        return RecipeCommentLike::create($data);
    }

    public function unLike($data)
    {
        return RecipeCommentLike::where('commentable_type',$data['commentable_type'])->where('commentable_id',$data['commentable_id'])->where('user_id',$data['user_id'])->delete();
    }

    public function comment($comment_id)
    {
        return RecipeComment::with('commentReplies.replies','commentLikes')->find($comment_id);
    }

    public function recentPosts($id)
    {
        return Recipe::with('currentLanguage')->where('id','!=',$id)->where('status','published')->latest()->paginate(5);
    }

    //API
    public function allRecipe($limit)
    {
        return Recipe::with('currentLanguage','products')->where('status','published')->latest()->paginate($limit);
    }


    public function search($term, $limit){
        return Recipe::whereHas('recipeLanguages', function ($q) use ($term) {
            $q->where(function ($query) use ($term) {
                $query->where('title', 'like', '%' . $term . '%')
                      ->orWhere('short_description', 'like', '%' . $term . '%')
                      ->orWhere('long_description', 'like', '%' . $term . '%');
            });
        })->where('status', 'published')
        ->latest()
        ->paginate($limit);
    }

    public function filterCategory($category_id,$limit){
        return Recipe::whereHas('recipeLanguages', function ($q) use ($category_id) {
            $q->where('category_id', $category_id);
        })->where('status', 'published')
        ->latest()
        ->paginate($limit);
    }

    public function ajaxRecipeFilter($term){

        return Recipe::whereHas('recipeLanguages', function ($q) use ($term) {
            $q->where('title', 'like', '%' . $term . '%');
        })->where('status','published')->select('id')
            ->limit(50)->get();
    }


}

