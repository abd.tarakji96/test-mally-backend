<?php

namespace App\Repositories\Admin;

use App\Models\Product;
use App\Models\Reward;
use App\Models\User;
use App\Models\RewardDetails;
use App\Models\SellerProfile;
use App\Repositories\Admin\Addon\WalletRepository;
use App\Repositories\Interfaces\Admin\RewardSystemInterface;
use Illuminate\Support\Facades\DB;
use App\CPU\NotifyHelper;

class RewardSystemRepository implements RewardSystemInterface{

    public function all()
    {
        return Product::with('productLanguages','user')->where('reward', '!=',0.00)->latest();
    }

    public function byUser($id,$limit)
    {
        return RewardDetails::with('product')->where('reward_id',$id)->paginate($limit);
    }

    public function paginate($limit)
    {
        return $this->all()->paginate($limit);
    }

    public function setRewardBy($request)
    {
        DB::beginTransaction();
        try {
            if($request->type == 'product' && $request->product_id != ''):
                foreach ($request->product_id as $product) {
                    $each_product = Product::where('id',$product)->first();
                    $each_product->reward = $request->reward;
                    $each_product->save();

                    DB::commit();
                    return true;
                }
            elseif($request->type == 'seller' && $request->seller_id != ''):
                $seller_id       = SellerProfile::find($request->seller_id)->user_id;
                $seller_products = Product::where('user_id',$seller_id)->get();
                foreach ($seller_products as $product) {
                    $product->reward = $request->reward;
                    $product->save();
                }

                DB::commit();
                return true;
            elseif($request->type == 'category' && $request->c != ''):
                $category_ids = [];
                if($request->has('sub_category')):
                    $category_ids   = \App\Utility\CategoryUtility::getMyAllChildIds($request->c);
                endif;
                $category_ids[] = (int)$request->c;
                $category_products = Product::whereIn('category_id',$category_ids)->get();
                foreach ($category_products as $product) {
                    $product->reward = $request->reward;
                    $product->save();
                }

                DB::commit();
                return true;
            endif;
        }catch (\Exception $e){
            DB::rollback();
            return false;
        }
    }

    public function updateReward($request)
    {
        DB::beginTransaction();
        try {
            $product = Product::where('id',$request->product_id)->first();
            $product->reward = $request->reward;
            $product->save();

            DB::commit();
            return true;
        }catch (\Exception $e){
            DB::rollback();
            return false;
        }
    }


    public function rewardUser($limit)
    {
        return Reward::with('user')->latest()->paginate($limit);
    }

    public function createReward($order)
    {

        $user = User::find($order->user_id);
        $reward = Reward::where('user_id',$user->id)->first();

        if (blank($reward))
        {
            $reward = Reward::create([
                'user_id' => $order->user_id,
                'rewards' => 0,
            ]);
        }

        $total_reward = 0;

        $reward_details = [];
        $order_details  = $order->orderDetails;

        foreach ($order_details as $key=> $order_detail)
        {
            $product        = $order_detail->product;
            $reward_point   = $product ? $product->reward : 0;
            $total_reward   += $reward_point * $order_detail->quantity;
            if ($reward_point > 0)
            {
                $reward_details[$key]   =[
                    'reward_id'         => $reward->id,
                    'product_id'        => @$product->id,
                    'product_qty'       => $order_detail->quantity,
                    'reward'            => $reward_point * $order_detail->quantity,
                    'created_at'        => now(),
                    'updated_at'        => now(),
                ];
            }

        }

        if($total_reward == 0){
            return $total_reward;
        }
        RewardDetails::insert($reward_details);

        $reward->rewards += $total_reward;
        $reward->save();
        $icon = settingHelper('favicon');
        
        $data = [
            'title' => "مبروك",
            'description' => "لقد ربحت ". $total_reward. " نقاط من خلال شراء منتجات خاصة",
            'image'       => static_asset($icon['image_57x57_url']),
        ];

        NotifyHelper::send_push_notif_to_device($user->cm_firebase_token, $data);
        return $reward;
    }

    public function createRewardInvitation($invitation_code)
    {
        $user = User::where('own_code' , $invitation_code)->first();
        if($user){
            $reward = Reward::where('user_id',$user->id)->first();

            if (blank($reward))
            {
                $reward = Reward::create([
                    'user_id' => $user->id,
                    'rewards' => 0,
                ]);
            }

            $total_reward = 0;

            $reward_details = [];
            $total_reward = settingHelper('rewords_for_inviting');
            $reward_details   = [
                'reward_id'         => $reward->id,
                'type'              => 0,
                'product_id'        => 0,
                'product_qty'       => 0,
                'reward'            => $total_reward,
                'created_at'        => now(),
                'updated_at'        => now(),
            ];

            RewardDetails::insert($reward_details);

            $reward->rewards += $total_reward;
            $reward->save();
            $icon = settingHelper('favicon');

            $data = [
                'title' => "مبروك",
                'description' => "لقد ربحت ". $total_reward. " نقاط من خلال تسجيل شخص من خلال كود الدعوة الخاص بك",
                'image'       => static_asset($icon['image_57x57_url']),
            ];
    
            NotifyHelper::send_push_notif_to_device($user->cm_firebase_token, $data);

            return $reward;
        }
        else{
            return 0;
        }
    }

    public function convertReward($data)
    {
        $reward                  = Reward::where('user_id',authId())->first();
        $reward->rewards        -= $data['reward'];
        $reward->last_converted  = now();
        $reward->save();

        $reward->user->balance  += $data['amount'];
        $reward->user->save();

        $wallet_repo             = new WalletRepository();

        $data                    = [
            'user_id'   => authId(),
            'amount'    => $data['amount'],
            'source'    => 'reward_point_convert',
            'type'      => 'income',
            'status'    => 'approved',
            'payment_method'     => 'system_automated',
            'payment_details'    => ['type'=> 'system_automated'],
        ];

        $wallet_repo->store($data);

        return $reward;
    }

    public function apiConvertReward($data, $userId, $reward)
    {
        $reward                  = Reward::where('user_id',$userId)->first();
        $reward->rewards        -= $data['reward'];
        $reward->last_converted  = now();
        $reward->save();

        $reward->user->balance  += $data['amount'];
        $reward->user->save();

        $wallet_repo             = new WalletRepository();

        $data                    = [
            'user_id'   => $userId,
            'amount'    => $data['amount'],
            'source'    => 'reward_point_convert',
            'type'      => 'income',
            'status'    => 'approved',
            'payment_method'     => 'system_automated',
            'payment_details'    => ['type'=> 'system_automated'],
        ];

        $wallet_repo->store($data);

        return $reward;
    }

    public function rewardHistory($id): array
    {
        $reward = Reward::where('user_id',$id)->first();
        
        $reward_details = '';

        if ($reward)
        {
            $reward_details = RewardDetails::with('product:id')->where('reward_id',$reward->id)->paginate(10);
        }

        return [
            'reward' => $reward,
            'reward_details' => $reward_details,
        ];
    }
}
