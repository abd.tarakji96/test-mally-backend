<?php

use App\Models\Setting;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('lang', 10)->default('en')->index()->nullable();
            $table->string('title',100)->index()->nullable();
            $table->mediumText('value')->nullable();
            $table->timestamps();
        });

        Setting::insert(['title' => 'default_country', 'value'  => '19', 'lang' => 'en']);
        Setting::insert(['title' => 'refund_protection_title', 'value'  => 'Mally eCommerce Refund Protection', 'lang' => 'en']);
        Setting::insert(['title' => 'refund_protection_sub_title', 'value'  => '30 Days cash back Guarantee', 'lang' => 'en']);
        Setting::insert(['title' => 'refund_policy_agreement', 'value'  => 'refund-policy', 'lang' => 'en']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
