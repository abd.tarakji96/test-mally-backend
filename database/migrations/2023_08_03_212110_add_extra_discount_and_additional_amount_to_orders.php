<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraDiscountAndAdditionalAmountToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('extra_discount')->after('discount')->default(0);
            $table->integer('additional_amount')->after('extra_discount')->default(0);
            $table->string('notes')->after('additional_amount')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('extra_discount');
            $table->dropColumn('additional_amount');
            $table->dropColumn('notes');
        });
    }
}
