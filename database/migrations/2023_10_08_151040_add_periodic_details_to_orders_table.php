<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPeriodicDetailsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->enum('order_type', ['normal','periodic'])->after('status')->default('normal');
            $table->enum('frequency', ['daily','days_of_week','days_of_month'])->after('order_type')->nullable();
            $table->json('days')->after('frequency')->nullable();
            $table->integer('time')->after('days')->nullable();
            $table->integer('remind_before')->after('time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('order_type');
            $table->dropColumn('frequency');
            $table->dropColumn('days');
            $table->dropColumn('time');
            $table->dropColumn('remind_before');
        });
    }
}
