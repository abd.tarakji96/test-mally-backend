<?php

namespace Database\Seeders\Admin;

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::create([ 'name' => 'Syrian Pounds', 'symbol' => 'S.P', 'code' => 'SYP', 'exchange_rate' => '1', 'status' => 1]);

    }
}
