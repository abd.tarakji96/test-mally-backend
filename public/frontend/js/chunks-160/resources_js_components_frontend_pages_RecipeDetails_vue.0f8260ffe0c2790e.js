"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_frontend_pages_RecipeDetails_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _partials_shimmer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../partials/shimmer */ "./resources/js/components/frontend/partials/shimmer.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import blog_comments from "./blog_partials/blog_comments";

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "blog_details",
  components: {
    //   blog_comments,
    shimmer: _partials_shimmer__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      collapeActive: 'categories',
      commentForm: {
        comment: '',
        blog_id: '',
        slug: this.$route.params.slug
      },
      ss: [],
      page: 1,
      category: true,
      recentPost: true,
      loading: false,
      share_dropdown: false
    };
  },
  mounted: function mounted() {
    console.log(this.blogDetails, 'pppppfff');
    this.$store.dispatch('recipeDetails', this.$route.params.slug);
    console.log(this.blogDetails, 'pppppfff');
  },
  computed: {
    blogDetails: function blogDetails() {
      return this.$store.getters.getRecipeDetails;
    },
    categories: function categories() {
      return this.$store.getters.getRecipeCategories;
    },
    tags: function tags() {
      return this.$store.getters.getRecipeTags;
    },
    recent_posts: function recent_posts() {
      return this.$store.getters.getRecentPosts;
    },
    ings: function ings() {
      return this.$store.getters.getIngs;
    },
    // comments() {
    //     return this.$store.getters.getBlogComments;
    // },
    shimmer: function shimmer() {
      return this.$store.state.module.shimmer;
    }
  },
  watch: {
    $route: function $route(to, from) {
      this.$store.dispatch('recipeDetails', this.$route.params.slug);
    }
  },
  methods: {
    collapeActiveStatus: function collapeActiveStatus(data) {
      if (this.collapeActive == data) {
        this.collapeActive = '';
      } else {
        this.collapeActive = data;
      }
    },
    comment: function comment() {
      var _this = this;

      this.loading = true;
      this.commentForm.blog_id = this.blogDetails.id;
      var url = this.getUrl('store/blog-comment');
      this.$Progress.start();
      axios.post(url, this.commentForm).then(function (response) {
        _this.loading = false;

        if (response.data.error) {
          toastr.error(response.data.error, _this.lang.Error + ' !!');
        } else {
          _this.commentForm.comment = '';

          _this.$store.dispatch('blogDetails', _this.$route.params.slug);

          if (response.data.success) {
            toastr.success(response.data.success, _this.lang.Success + ' !!');
          }

          _this.$Progress.finish();
        }
      })["catch"](function (error) {
        _this.loading = false;
      });
    },
    loadCategories: function loadCategories() {
      var _this2 = this;

      this.page++;
      var url = this.url + '/load/blog-categories?page=' + this.page;
      this.$Progress.start();
      axios.get(url).then(function (response) {
        if (response.data.error) {
          toastr.error(response.data.error, _this2.lang.Error + ' !!');
        } else {
          var categories = response.data.categories.data;

          if (categories.length > 0) {
            for (var i in categories) {
              _this2.categories.data.push(categories[i]);
            }
          }

          _this2.$Progress.finish();

          _this2.categories.next_page_url = response.data.categories.next_page_url;
        }
      });
    },
    shareDropdown: function shareDropdown() {
      var _this3 = this;

      this.share_dropdown = !this.share_dropdown;
      this.share_dropdown && this.$nextTick(function () {
        document.addEventListener('click', _this3.hideLanguageDropdown);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "shimmer.vue",
  props: ['height'],
  data: function data() {
    return {
      style: {
        height: this.height + 'px'
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.nn span[data-v-5f1f55f7]{\r\n    color:blue\n}\r\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_style_index_0_id_5f1f55f7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_style_index_0_id_5f1f55f7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_style_index_0_id_5f1f55f7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./resources/js/components/frontend/pages/RecipeDetails.vue":
/*!******************************************************************!*\
  !*** ./resources/js/components/frontend/pages/RecipeDetails.vue ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _RecipeDetails_vue_vue_type_template_id_5f1f55f7_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true& */ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true&");
/* harmony import */ var _RecipeDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RecipeDetails.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js&");
/* harmony import */ var _RecipeDetails_vue_vue_type_style_index_0_id_5f1f55f7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css& */ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _RecipeDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RecipeDetails_vue_vue_type_template_id_5f1f55f7_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _RecipeDetails_vue_vue_type_template_id_5f1f55f7_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "5f1f55f7",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/pages/RecipeDetails.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/partials/shimmer.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/frontend/partials/shimmer.vue ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shimmer.vue?vue&type=template&id=44ada926& */ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&");
/* harmony import */ var _shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shimmer.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.render,
  _shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/partials/shimmer.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RecipeDetails.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./shimmer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css&":
/*!***************************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css& ***!
  \***************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_style_index_0_id_5f1f55f7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css&");


/***/ }),

/***/ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true& ***!
  \*************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_template_id_5f1f55f7_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_template_id_5f1f55f7_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_template_id_5f1f55f7_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true&");


/***/ }),

/***/ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926& ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./shimmer.vue?vue&type=template&id=44ada926& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "sg-page-content" }, [
    _c("section", { staticClass: "grid-view-tab" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _vm.lengthCounter(_vm.blogDetails) > 0
            ? _c("div", { staticClass: "col-md-4 col-lg-3" }, [
                _c("div", { staticClass: "sg-sitebar new-shop-sitebar" }, [
                  _c(
                    "div",
                    {
                      staticClass: "accordion",
                      attrs: { id: "accordionExample" },
                    },
                    [
                      _c("div", { staticClass: "accordion-item" }, [
                        _c(
                          "div",
                          {
                            staticClass: "accordion-header",
                            attrs: { id: "ac1" },
                          },
                          [
                            _c(
                              "button",
                              {
                                staticClass: "accordion-button",
                                class: { collapsed: !_vm.category },
                                attrs: {
                                  type: "button",
                                  "data-bs-toggle": "collapse",
                                  "data-bs-target": "#collapse1",
                                  "aria-expanded": "true",
                                  "aria-controls": "collapse1",
                                },
                                on: {
                                  click: function ($event) {
                                    _vm.category = !_vm.category
                                  },
                                },
                              },
                              [
                                _vm._v(
                                  "All Categories\n                                    "
                                ),
                              ]
                            ),
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "accordion-collapse collapse",
                            class: { show: _vm.category },
                            attrs: {
                              id: "collapse1",
                              "aria-labelledby": "ac1",
                              "data-bs-parent": "#accordionExample",
                            },
                          },
                          [
                            _c("div", { staticClass: "accordion-body" }, [
                              _c(
                                "ul",
                                { staticClass: "global-list" },
                                [
                                  _vm._l(
                                    _vm.categories.data,
                                    function (category, index) {
                                      return _c(
                                        "li",
                                        { key: index },
                                        [
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "category.blogs",
                                                  params: {
                                                    slug: category.slug,
                                                  },
                                                },
                                              },
                                            },
                                            [
                                              _vm._v(
                                                "\n                                                    " +
                                                  _vm._s(category.title) +
                                                  "\n                                                "
                                              ),
                                            ]
                                          ),
                                        ],
                                        1
                                      )
                                    }
                                  ),
                                  _vm._v(" "),
                                  _vm.categories.next_page_url
                                    ? _c("li", [
                                        _c(
                                          "a",
                                          {
                                            attrs: {
                                              href: "javascript:void(0)",
                                            },
                                            on: { click: _vm.loadCategories },
                                          },
                                          [_vm._v(_vm._s(_vm.lang.show_more))]
                                        ),
                                      ])
                                    : _vm._e(),
                                ],
                                2
                              ),
                            ]),
                          ]
                        ),
                      ]),
                      _vm._v(" "),
                      _vm.tags[1] != null
                        ? _c("div", { staticClass: "accordion-item" }, [
                            _c(
                              "div",
                              {
                                staticClass: "accordion-header",
                                attrs: { id: "ac3" },
                              },
                              [
                                _c(
                                  "button",
                                  {
                                    staticClass: "accordion-button",
                                    class: {
                                      collapsed: _vm.collapeActive != "tags",
                                    },
                                    attrs: {
                                      type: "button",
                                      "data-bs-toggle": "collapse",
                                      "data-bs-target": "#collapse3",
                                      "aria-expanded": "false",
                                      "aria-controls": "collapse3",
                                    },
                                    on: {
                                      click: function ($event) {
                                        return _vm.collapeActiveStatus("tags")
                                      },
                                    },
                                  },
                                  [
                                    _vm._v(
                                      _vm._s(_vm.lang.tags) +
                                        "\n                                    "
                                    ),
                                  ]
                                ),
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass: "accordion-collapse collapse",
                                class: { show: _vm.collapeActive == "tags" },
                                attrs: {
                                  id: "collapse3",
                                  "aria-labelledby": "ac3",
                                  "data-bs-parent": "#accordionExample",
                                },
                              },
                              [
                                _c("div", { staticClass: "accordion-body" }, [
                                  _c(
                                    "div",
                                    { staticClass: "tagcloud" },
                                    _vm._l(_vm.tags, function (tag, i) {
                                      return _c(
                                        "a",
                                        {
                                          key: i,
                                          attrs: { href: "javascript:void(0)" },
                                        },
                                        [_vm._v(_vm._s(tag))]
                                      )
                                    }),
                                    0
                                  ),
                                ]),
                              ]
                            ),
                          ])
                        : _vm._e(),
                    ]
                  ),
                ]),
              ])
            : _vm.shimmer
            ? _c(
                "div",
                { staticClass: "col-md-4 col-lg-3" },
                [_c("shimmer", { attrs: { height: 600 } })],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-8 col-lg-9" }, [
            _c("div", { staticClass: "blog-details" }, [
              _c(
                "div",
                {
                  staticClass: "post",
                  staticStyle: {
                    "border-bottom": "1px solid #660F1F",
                    "padding-bottom": "20px",
                  },
                },
                [
                  _c("div", { staticClass: "entry-header" }, [
                    _c("div", { staticClass: "entry-thumbnail" }, [
                      _c(
                        "a",
                        [
                          _vm.blogDetails.banner_img
                            ? _c("img", {
                                staticClass: "img-fluid",
                                attrs: {
                                  loading: "lazy",
                                  src: _vm.blogDetails.banner_img,
                                  alt: _vm.blogDetails.title,
                                },
                              })
                            : _vm.shimmer
                            ? _c("shimmer", { attrs: { height: 200 } })
                            : _vm._e(),
                        ],
                        1
                      ),
                    ]),
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "entry-content" }, [
                    _c("div", { staticClass: "entry-meta" }, [
                      _vm.lengthCounter(_vm.blogDetails) > 0
                        ? _c("ul", { staticClass: "global-list" }, [
                            _vm.blogDetails.user
                              ? _c("li", [
                                  _c(
                                    "a",
                                    { attrs: { href: "javascript:void(0)" } },
                                    [
                                      _vm.blogDetails.user.user_profile_image
                                        ? _c("img", {
                                            staticClass: "img-fluid",
                                            attrs: {
                                              loading: "lazy",
                                              src: _vm.blogDetails.user
                                                .user_profile_image,
                                              alt: _vm.blogDetails.user
                                                .full_name,
                                            },
                                          })
                                        : _vm._e(),
                                      _vm._v(
                                        _vm._s(_vm.blogDetails.user.full_name)
                                      ),
                                    ]
                                  ),
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _c("li", [
                              _c(
                                "a",
                                { attrs: { href: "javascript:void(0)" } },
                                [
                                  _c("span", {
                                    staticClass: "mdi mdi-calendar-month",
                                  }),
                                  _vm._v(
                                    _vm._s(_vm.blogDetails.published_date)
                                  ),
                                ]
                              ),
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("span", {
                                  staticClass: "mdi mdi-eye-outline",
                                }),
                                _vm._v(
                                  _vm._s(_vm.blogDetails.blog_views_count) +
                                    " " +
                                    _vm._s(_vm.lang.view)
                                ),
                              ]),
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("div", { staticClass: "dropdown" }, [
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "btn btn-secondary share_dropdown",
                                    class: { show: _vm.share_dropdown },
                                    attrs: {
                                      type: "button",
                                      id: "dropdownMenuButton",
                                      "data-toggle": "dropdown",
                                      "aria-haspopup": "true",
                                      "aria-expanded": "false",
                                    },
                                    on: {
                                      click: function ($event) {
                                        $event.stopPropagation()
                                        return _vm.shareDropdown.apply(
                                          null,
                                          arguments
                                        )
                                      },
                                    },
                                  },
                                  [
                                    _c("span", {
                                      staticClass: "mdi mdi-share-variant",
                                    }),
                                    _vm._v(
                                      _vm._s(_vm.lang.share) +
                                        "\n                                                "
                                    ),
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "dropdown-menu",
                                    class: { show: _vm.share_dropdown },
                                    attrs: {
                                      "aria-labelledby": "dropdownMenuButton",
                                    },
                                    on: {
                                      click: function ($event) {
                                        _vm.share_dropdown = false
                                      },
                                    },
                                  },
                                  [
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          target: "_blank",
                                          href:
                                            "https://www.facebook.com/sharer/sharer.php?u=" +
                                            _vm.getUrl(
                                              "blog/" + _vm.blogDetails.slug
                                            ),
                                        },
                                      },
                                      [_vm._v(_vm._s(_vm.lang.facebook))]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          target: "_blank",
                                          href:
                                            "https://twitter.com/intent/tweet?text=" +
                                            _vm.blogDetails.title +
                                            "&url=" +
                                            _vm.getUrl(
                                              "blog/" + _vm.blogDetails.slug
                                            ),
                                        },
                                      },
                                      [_vm._v(_vm._s(_vm.lang.twitter))]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          target: "_blank",
                                          href:
                                            "https://www.linkedin.com/sharing/share-offsite?mini=true&url=" +
                                            _vm.getUrl(
                                              "blog/" + _vm.blogDetails.slug
                                            ) +
                                            "&title=" +
                                            _vm.blogDetails.title +
                                            "&summary=Extra+linkedin+summary+can+be+passed+here",
                                        },
                                      },
                                      [_vm._v(_vm._s(_vm.lang.linkedin))]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          target: "_blank",
                                          href:
                                            "https://wa.me/?text=" +
                                            _vm.getUrl(
                                              "blog/" + _vm.blogDetails.slug
                                            ),
                                        },
                                      },
                                      [_vm._v(_vm._s(_vm.lang.whatsapp))]
                                    ),
                                  ]
                                ),
                              ]),
                            ]),
                          ])
                        : _vm.shimmer
                        ? _c(
                            "ul",
                            { staticClass: "global-list" },
                            [_c("shimmer", { attrs: { height: 10 } })],
                            1
                          )
                        : _vm._e(),
                    ]),
                    _vm._v(" "),
                    _vm.lengthCounter(_vm.blogDetails) > 0
                      ? _c("div", [
                          _c("h1", { staticClass: "entry-title" }, [
                            _vm._v(_vm._s(_vm.blogDetails.title)),
                          ]),
                          _vm._v(" "),
                          _c("div", {
                            staticClass: "nn",
                            domProps: {
                              innerHTML: _vm._s(_vm.blogDetails.description),
                            },
                          }),
                        ])
                      : _vm.shimmer
                      ? _c("div", [_c("shimmer", { attrs: { height: 30 } })], 1)
                      : _vm._e(),
                  ]),
                ]
              ),
              _vm._v(" "),
              _vm.lengthCounter(_vm.ings) > 0
                ? _c(
                    "div",
                    {
                      staticStyle: {
                        "margin-top": "20px",
                        "border-bottom": "1px solid #660F1F",
                        "padding-bottom": "40px",
                      },
                    },
                    [
                      _c(
                        "span",
                        {
                          staticStyle: {
                            color: "#66101F",
                            "font-size": "20px",
                            "font-weight": "600",
                          },
                        },
                        [_vm._v("Ingredients List")]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "row mt-3 mb-4" },
                        _vm._l(_vm.ings, function (ing) {
                          return _c(
                            "div",
                            {
                              key: ing,
                              staticClass: "col-md-4 mt-3 check",
                              staticStyle: {
                                display: "flex",
                                "align-items": "center",
                                "justify-content": "center",
                                gap: "3px",
                                "flex-direction": "row-reverse",
                              },
                            },
                            [
                              _c(
                                "label",
                                {
                                  staticClass: "register-info-text",
                                  staticStyle: {
                                    margin: "0 6px",
                                    display: "flex",
                                    "align-items": "center",
                                    "flex-direction": "column",
                                  },
                                },
                                [
                                  _c(
                                    "span",
                                    { staticStyle: { color: "#660F1F" } },
                                    [_vm._v(_vm._s(ing.product_name))]
                                  ),
                                  _c(
                                    "span",
                                    { staticStyle: { color: "#660F1F" } },
                                    [_vm._v(_vm._s(ing.price))]
                                  ),
                                ]
                              ),
                              _vm._v(" "),
                              _c("input", {
                                staticStyle: {
                                  height: "55px",
                                  width: "55px",
                                  "border-radius": "10px",
                                  "font-size": "16px",
                                  "background-color": "#FFFAF5",
                                },
                                attrs: { type: "number" },
                              }),
                              _vm._v(" "),
                              _c("input", {
                                staticStyle: {
                                  height: "20px",
                                  width: "30px",
                                  border: "2px solid #660F1F !important",
                                  "background-color": "#FFFAF5",
                                },
                                attrs: { type: "checkbox" },
                              }),
                            ]
                          )
                        }),
                        0
                      ),
                      _vm._v(" "),
                      _vm.lengthCounter(_vm.blogDetails) > 0
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-primary",
                              staticStyle: {
                                "border-radius": "10px",
                                height: "40px",
                              },
                            },
                            [
                              _c(
                                "svg",
                                {
                                  attrs: {
                                    width: "17",
                                    height: "17",
                                    viewBox: "0 0 48 44",
                                    fill: "none",
                                    xmlns: "http://www.w3.org/2000/svg",
                                  },
                                },
                                [
                                  _c("path", {
                                    attrs: {
                                      d: "M13.7037 6.41667H46.1667L41.75 21.875H16.082M43.9583 30.7083H17.4583L13.0417 2H6.41667M6.41667 13.0417H2M8.625 19.6667H2M10.8333 26.2917H2M19.6667 39.5417C19.6667 40.7613 18.678 41.75 17.4583 41.75C16.2387 41.75 15.25 40.7613 15.25 39.5417C15.25 38.322 16.2387 37.3333 17.4583 37.3333C18.678 37.3333 19.6667 38.322 19.6667 39.5417ZM43.9583 39.5417C43.9583 40.7613 42.9697 41.75 41.75 41.75C40.5303 41.75 39.5417 40.7613 39.5417 39.5417C39.5417 38.322 40.5303 37.3333 41.75 37.3333C42.9697 37.3333 43.9583 38.322 43.9583 39.5417Z",
                                      stroke: "#FFFAF5",
                                      "stroke-width": "3",
                                      "stroke-linecap": "round",
                                      "stroke-linejoin": "round",
                                    },
                                  }),
                                ]
                              ),
                              _vm._v(
                                "\n                                Add Selected Item To Cart\n                            "
                              ),
                            ]
                          )
                        : _vm._e(),
                    ]
                  )
                : _vm.shimmer
                ? _c("div", [_c("shimmer", { attrs: { height: 30 } })], 1)
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "comment-area" }),
              _vm._v(" "),
              _vm.lengthCounter(_vm.blogDetails) > 0
                ? _c("div", [
                    _vm.authUser
                      ? _c("div", { staticClass: "comment-form" }, [
                          _c("h3", { staticClass: "title-style-1" }, [
                            _vm._v(_vm._s(_vm.lang.write_a_comments)),
                          ]),
                          _vm._v(" "),
                          _c(
                            "form",
                            {
                              staticClass: "tr-form",
                              on: {
                                submit: function ($event) {
                                  $event.preventDefault()
                                  return _vm.comment.apply(null, arguments)
                                },
                              },
                            },
                            [
                              _c("div", { staticClass: "form-group" }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.commentForm.comment,
                                      expression: "commentForm.comment",
                                    },
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    required: "required",
                                    rows: "8",
                                    placeholder: "Write Comment",
                                  },
                                  domProps: { value: _vm.commentForm.comment },
                                  on: {
                                    input: function ($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.commentForm,
                                        "comment",
                                        $event.target.value
                                      )
                                    },
                                  },
                                }),
                              ]),
                              _vm._v(" "),
                              _vm.loading
                                ? _c("loading_button", {
                                    attrs: { class_name: "btn btn-primary" },
                                  })
                                : _c("input", {
                                    staticClass: "btn btn-primary",
                                    attrs: { type: "submit" },
                                    domProps: { value: _vm.lang.post },
                                  }),
                            ],
                            1
                          ),
                        ])
                      : _vm._e(),
                  ])
                : _vm.shimmer
                ? _c(
                    "div",
                    { staticClass: "comment-form" },
                    [_c("shimmer", { attrs: { height: 200 } })],
                    1
                  )
                : _vm._e(),
            ]),
          ]),
        ]),
      ]),
    ]),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("img", {
    staticClass: "shimmer",
    style: [_vm.height ? _vm.style : null],
    attrs: {
      src: _vm.getUrl("public/images/default/preview.jpg"),
      alt: "shimmer",
    },
  })
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);