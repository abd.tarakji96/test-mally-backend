(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_frontend_pages_RecipeDetails_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _blog_partials_blog_comments__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./blog_partials/blog_comments */ "./resources/js/components/frontend/pages/blog_partials/blog_comments.vue");
/* harmony import */ var _partials_shimmer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../partials/shimmer */ "./resources/js/components/frontend/partials/shimmer.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "RecipeDetails",
  components: {
    blog_comments: _blog_partials_blog_comments__WEBPACK_IMPORTED_MODULE_0__["default"],
    shimmer: _partials_shimmer__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      collapeActive: 'categories',
      commentForm: {
        comment: '',
        blog_id: '',
        slug: this.$route.params.slug
      },
      page: 1,
      category: true,
      recentPost: true,
      loading: false,
      share_dropdown: false
    };
  },
  mounted: function mounted() {
    this.$store.dispatch('recipeDetails', this.$route.params.slug);
  },
  computed: {
    blogDetails: function blogDetails() {
      console.log(this.$store.getters.getRecipeDetails, 'pppuuud');
      return this.$store.getters.RecipeDetails;
    },
    categories: function categories() {
      return this.$store.getters.getBlogCategories;
    },
    tags: function tags() {
      return this.$store.getters.getBlogTags;
    },
    recent_posts: function recent_posts() {
      return this.$store.getters.getRecentPosts;
    },
    comments: function comments() {
      return this.$store.getters.getBlogComments;
    },
    shimmer: function shimmer() {
      return this.$store.state.module.shimmer;
    }
  },
  watch: {
    $route: function $route(to, from) {
      this.$store.dispatch('RecipeDetails', this.$route.params.slug);
    }
  },
  methods: {
    collapeActiveStatus: function collapeActiveStatus(data) {
      if (this.collapeActive == data) {
        this.collapeActive = '';
      } else {
        this.collapeActive = data;
      }
    },
    comment: function comment() {
      var _this = this;

      this.loading = true;
      this.commentForm.blog_id = this.blogDetails.id;
      var url = this.getUrl('store/blog-comment');
      this.$Progress.start();
      axios.post(url, this.commentForm).then(function (response) {
        _this.loading = false;

        if (response.data.error) {
          toastr.error(response.data.error, _this.lang.Error + ' !!');
        } else {
          _this.commentForm.comment = '';

          _this.$store.dispatch('RecipeDetails', _this.$route.params.slug);

          if (response.data.success) {
            toastr.success(response.data.success, _this.lang.Success + ' !!');
          }

          _this.$Progress.finish();
        }
      })["catch"](function (error) {
        _this.loading = false;
      });
    },
    loadCategories: function loadCategories() {
      var _this2 = this;

      this.page++;
      var url = this.url + '/load/blog-categories?page=' + this.page;
      this.$Progress.start();
      axios.get(url).then(function (response) {
        if (response.data.error) {
          toastr.error(response.data.error, _this2.lang.Error + ' !!');
        } else {
          var categories = response.data.categories.data;

          if (categories.length > 0) {
            for (var i in categories) {
              _this2.categories.data.push(categories[i]);
            }
          }

          _this2.$Progress.finish();

          _this2.categories.next_page_url = response.data.categories.next_page_url;
        }
      });
    },
    shareDropdown: function shareDropdown() {
      var _this3 = this;

      this.share_dropdown = !this.share_dropdown;
      this.share_dropdown && this.$nextTick(function () {
        document.addEventListener('click', _this3.hideLanguageDropdown);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/blog_comments.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/blog_comments.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _blog_reply__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./blog_reply */ "./resources/js/components/frontend/pages/blog_partials/blog_reply.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "blog_comments",
  components: {
    blog_reply: _blog_reply__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: ['comments', 'blogDetails'],
  data: function data() {
    return {
      comment_reply: '',
      page: 1,
      show_replies: '',
      loading: false,
      replyForm: {
        comment: '',
        blog_comment_id: '',
        parent_id: '',
        level: 0,
        slug: this.$route.params.slug
      }
    };
  },
  methods: {
    commentReply: function commentReply(id, parent_id, level) {
      var _this = this;

      this.loading = true;
      this.replyForm.blog_comment_id = id;

      if (parent_id) {
        this.replyForm.parent_id = parent_id;
      }

      if (level || level == 0) {
        this.replyForm.level = level + 1;
      }

      var url = this.getUrl('store/blog-comment-reply');
      axios.post(url, this.replyForm).then(function (response) {
        _this.loading = false;

        if (response.data.error) {
          toastr.error(response.data.error, _this.lang.Error + ' !!');
        } else {
          _this.replyForm.comment = '';

          _this.$store.dispatch('blogDetails', _this.$route.params.slug);

          _this.comment_reply = '';

          if (response.data.success) {
            toastr.success(response.data.success, _this.lang.Success + ' !!');
          }
        }
      })["catch"](function (error) {
        _this.loading = false;
      });
    },
    loadMoreComments: function loadMoreComments() {
      var _this2 = this;

      this.page++;
      var url = this.getUrl('load/blog-comments/' + this.blogDetails.id + '?page=' + this.page);
      axios.get(url).then(function (response) {
        if (response.data.error) {
          toastr.error(response.data.error, _this2.lang.Error + ' !!');
        } else {
          var comments = response.data.comments.data;

          if (comments.length > 0) {
            for (var i = 0; i < comments.length; i++) {
              _this2.comments.data.push(comments[i]);
            }
          }
        }

        _this2.comments.next_page_url = response.data.comments.next_page_url;
      });
    },
    showReplyArea: function showReplyArea(id) {
      if (this.show_replies == id) {
        this.show_replies = '';
      } else {
        this.show_replies = id;
      }
    },
    commentLike: function commentLike(id) {
      var _this3 = this;

      var data = {
        paginate: this.page,
        id: id,
        blog_id: this.blogDetails.id
      };
      this.blog_like_loading = true;
      var url = this.getUrl('blog/like-comments');
      axios.post(url, data).then(function (response) {
        _this3.blog_like_loading = false;

        if (response.data.error) {
          toastr.error(response.data.error, _this3.lang.Error + ' !!');
        } else {
          _this3.comments.data = response.data.comments.data;
          _this3.comments.next_page_url = response.data.comments.next_page_url;
          _this3.comments.total = response.data.comments.total;

          if (response.data.success) {
            toastr.success(response.data.success, _this3.lang.Success + ' !!');
          }
        }
      })["catch"](function (error) {
        _this3.blog_like_loading = false;
      });
    },
    unLike: function unLike(id) {
      var _this4 = this;

      var data = {
        paginate: this.page,
        id: id,
        blog_id: this.blogDetails.id
      };
      this.blog_like_loading = true;
      var url = this.getUrl('blog/unlike-comments');
      axios.post(url, data).then(function (response) {
        _this4.blog_like_loading = false;

        if (response.data.error) {
          toastr.error(response.data.error, _this4.lang.Error + ' !!');
        } else {
          _this4.comments.data = response.data.comments.data;
          _this4.comments.next_page_url = response.data.comments.next_page_url;
          _this4.comments.total = response.data.comments.total;

          if (response.data.success) {
            toastr.success(response.data.success, _this4.lang.Success + ' !!');
          }
        }
      })["catch"](function (error) {
        _this4.blog_like_loading = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/blog_reply.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/blog_reply.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _nested_reply__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./nested_reply */ "./resources/js/components/frontend/pages/blog_partials/nested_reply.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "blog_reply",
  components: {
    nested_reply: _nested_reply__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: ['comment', 'blogDetails', 'page'],
  computed: {
    replyFormId: function replyFormId() {
      return this.$store.getters.getReplyForm;
    }
  },
  data: function data() {
    return {
      loading: false,
      replyForm: {
        comment: '',
        blog_comment_id: '',
        parent_id: '',
        level: 0,
        slug: this.$route.params.slug
      }
    };
  },
  methods: {
    commentReply: function commentReply(id, parent_id, level) {
      var _this = this;

      this.loading = true;
      this.replyForm.blog_comment_id = id;

      if (parent_id) {
        // this.replyForm.parent_id = parent_id;
        this.replyForm.blog_comment_id = id;
        this.replyForm.level = 0;
      } // if (level || level == 0) {
      //     this.replyForm.level = level + 1;
      // }


      var url = this.getUrl('store/blog-comment-reply');
      axios.post(url, this.replyForm).then(function (response) {
        _this.loading = false;

        if (response.data.error) {
          toastr.error(response.data.error, _this.lang.Error + ' !!');
        } else {
          _this.replyForm.comment = '';

          _this.$store.dispatch('blogDetails', _this.$route.params.slug);

          _this.$store.dispatch('replyForm', '');

          if (response.data.success) {
            toastr.success(response.data.success, _this.lang.Success + ' !!');
          }
        }
      })["catch"](function (error) {
        _this.loading = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/nested_reply.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/nested_reply.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "nested_reply",
  props: ['reply', 'comment', 'reply_reply_area'],
  data: function data() {
    return {
      loading: false,
      replyForm: {
        comment: '',
        blog_comment_id: '',
        parent_id: '',
        level: 0,
        slug: this.$route.params.slug
      }
    };
  },
  components: {},
  computed: {
    replyFormId: function replyFormId() {
      return this.$store.getters.getReplyForm;
    }
  },
  methods: {
    commentReply: function commentReply(id, parent_id, level) {
      var _this = this;

      this.loading = true;
      this.replyForm.blog_comment_id = id;

      if (parent_id) {
        this.replyForm.parent_id = parent_id;
      }

      if (level || level == 0) {
        this.replyForm.level = level + 1;
      }

      var url = this.getUrl('store/blog-comment-reply');
      axios.post(url, this.replyForm).then(function (response) {
        _this.loading = false;

        if (response.data.error) {
          toastr.error(response.data.error, _this.lang.Error + ' !!');
        } else {
          _this.replyForm.comment = '';

          _this.$store.dispatch('blogDetails', _this.$route.params.slug);

          _this.$store.dispatch('replyForm', '');

          if (response.data.success) {
            toastr.success(response.data.success, _this.lang.Success + ' !!');
          }
        }
      })["catch"](function (error) {
        _this.loading = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "shimmer.vue",
  props: ['height'],
  data: function data() {
    return {
      style: {
        height: this.height + 'px'
      }
    };
  }
});

/***/ }),

/***/ "./resources/js/components/frontend/pages/RecipeDetails.vue":
/*!******************************************************************!*\
  !*** ./resources/js/components/frontend/pages/RecipeDetails.vue ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _RecipeDetails_vue_vue_type_template_id_5f1f55f7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RecipeDetails.vue?vue&type=template&id=5f1f55f7& */ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&");
/* harmony import */ var _RecipeDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RecipeDetails.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RecipeDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RecipeDetails_vue_vue_type_template_id_5f1f55f7___WEBPACK_IMPORTED_MODULE_0__.render,
  _RecipeDetails_vue_vue_type_template_id_5f1f55f7___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/pages/RecipeDetails.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/pages/blog_partials/blog_comments.vue":
/*!********************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/blog_partials/blog_comments.vue ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _blog_comments_vue_vue_type_template_id_65237274___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./blog_comments.vue?vue&type=template&id=65237274& */ "./resources/js/components/frontend/pages/blog_partials/blog_comments.vue?vue&type=template&id=65237274&");
/* harmony import */ var _blog_comments_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./blog_comments.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/pages/blog_partials/blog_comments.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _blog_comments_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _blog_comments_vue_vue_type_template_id_65237274___WEBPACK_IMPORTED_MODULE_0__.render,
  _blog_comments_vue_vue_type_template_id_65237274___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/pages/blog_partials/blog_comments.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/pages/blog_partials/blog_reply.vue":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/blog_partials/blog_reply.vue ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _blog_reply_vue_vue_type_template_id_0dd40d9a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./blog_reply.vue?vue&type=template&id=0dd40d9a& */ "./resources/js/components/frontend/pages/blog_partials/blog_reply.vue?vue&type=template&id=0dd40d9a&");
/* harmony import */ var _blog_reply_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./blog_reply.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/pages/blog_partials/blog_reply.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _blog_reply_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _blog_reply_vue_vue_type_template_id_0dd40d9a___WEBPACK_IMPORTED_MODULE_0__.render,
  _blog_reply_vue_vue_type_template_id_0dd40d9a___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/pages/blog_partials/blog_reply.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/pages/blog_partials/nested_reply.vue":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/blog_partials/nested_reply.vue ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _nested_reply_vue_vue_type_template_id_4bea6122___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./nested_reply.vue?vue&type=template&id=4bea6122& */ "./resources/js/components/frontend/pages/blog_partials/nested_reply.vue?vue&type=template&id=4bea6122&");
/* harmony import */ var _nested_reply_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./nested_reply.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/pages/blog_partials/nested_reply.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _nested_reply_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _nested_reply_vue_vue_type_template_id_4bea6122___WEBPACK_IMPORTED_MODULE_0__.render,
  _nested_reply_vue_vue_type_template_id_4bea6122___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/pages/blog_partials/nested_reply.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/partials/shimmer.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/frontend/partials/shimmer.vue ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shimmer.vue?vue&type=template&id=44ada926& */ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&");
/* harmony import */ var _shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shimmer.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.render,
  _shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/partials/shimmer.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RecipeDetails.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/pages/blog_partials/blog_comments.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/blog_partials/blog_comments.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_comments_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./blog_comments.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/blog_comments.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_comments_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/pages/blog_partials/blog_reply.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/blog_partials/blog_reply.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_reply_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./blog_reply.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/blog_reply.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_reply_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/pages/blog_partials/nested_reply.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/blog_partials/nested_reply.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_nested_reply_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./nested_reply.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/nested_reply.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_nested_reply_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./shimmer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7& ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_template_id_5f1f55f7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RecipeDetails.vue?vue&type=template&id=5f1f55f7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&");


/***/ }),

/***/ "./resources/js/components/frontend/pages/blog_partials/blog_comments.vue?vue&type=template&id=65237274&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/blog_partials/blog_comments.vue?vue&type=template&id=65237274& ***!
  \***************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_comments_vue_vue_type_template_id_65237274___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_comments_vue_vue_type_template_id_65237274___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_comments_vue_vue_type_template_id_65237274___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./blog_comments.vue?vue&type=template&id=65237274& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/blog_comments.vue?vue&type=template&id=65237274&");


/***/ }),

/***/ "./resources/js/components/frontend/pages/blog_partials/blog_reply.vue?vue&type=template&id=0dd40d9a&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/blog_partials/blog_reply.vue?vue&type=template&id=0dd40d9a& ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_reply_vue_vue_type_template_id_0dd40d9a___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_reply_vue_vue_type_template_id_0dd40d9a___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_blog_reply_vue_vue_type_template_id_0dd40d9a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./blog_reply.vue?vue&type=template&id=0dd40d9a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/blog_reply.vue?vue&type=template&id=0dd40d9a&");


/***/ }),

/***/ "./resources/js/components/frontend/pages/blog_partials/nested_reply.vue?vue&type=template&id=4bea6122&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/blog_partials/nested_reply.vue?vue&type=template&id=4bea6122& ***!
  \**************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_nested_reply_vue_vue_type_template_id_4bea6122___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_nested_reply_vue_vue_type_template_id_4bea6122___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_nested_reply_vue_vue_type_template_id_4bea6122___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./nested_reply.vue?vue&type=template&id=4bea6122& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/nested_reply.vue?vue&type=template&id=4bea6122&");


/***/ }),

/***/ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926& ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./shimmer.vue?vue&type=template&id=44ada926& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/***/ (() => {

throw new Error("Module build failed (from ./node_modules/vue-loader/lib/loaders/templateLoader.js):\nSyntaxError: Unexpected token (1:4489)\n    at Parser.pp$4.raise (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:2757:13)\n    at Parser.pp.unexpected (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:647:8)\n    at Parser.pp$3.parseExprAtom (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:2196:10)\n    at Parser.<anonymous> (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:6003:24)\n    at Parser.parseExprAtom (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:6129:31)\n    at Parser.pp$3.parseExprSubscripts (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:2047:19)\n    at Parser.pp$3.parseMaybeUnary (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:2024:17)\n    at Parser.pp$3.parseExprOps (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:1966:19)\n    at Parser.pp$3.parseMaybeConditional (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:1949:19)\n    at Parser.pp$3.parseMaybeAssign (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:1925:19)\n    at Parser.pp$3.parseMaybeConditional (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:1954:28)\n    at Parser.pp$3.parseMaybeAssign (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:1925:19)\n    at Parser.pp$3.parseParenAndDistinguishExpression (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:2238:30)\n    at Parser.pp$3.parseExprAtom (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:2163:41)\n    at Parser.<anonymous> (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:6003:24)\n    at Parser.parseExprAtom (C:\\vue\\mally\\test-mally-backend\\node_modules\\vue-template-es2015-compiler\\buble.js:6129:31)");

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/blog_comments.vue?vue&type=template&id=65237274&":
/*!******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/blog_comments.vue?vue&type=template&id=65237274& ***!
  \******************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.comments.data
    ? _c(
        "ul",
        { staticClass: "comment-list global-list" },
        [
          _vm._l(_vm.comments.data, function (comment, index) {
            return _c("li", { key: index }, [
              _c(
                "div",
                { staticClass: "comment_info" },
                [
                  comment.user
                    ? _c(
                        "div",
                        { staticClass: "commenter-avatar" },
                        [
                          _c(
                            "router-link",
                            { attrs: { to: { name: "profile" } } },
                            [
                              _c("img", {
                                staticClass: "img-fluid",
                                attrs: {
                                  src: comment.user.profile_image,
                                  alt: comment.user.full_name,
                                },
                              }),
                            ]
                          ),
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  comment.user
                    ? _c("div", { staticClass: "comment-box" }, [
                        _c("div", { staticClass: "comment-title" }, [
                          _c(
                            "span",
                            { staticClass: "title-1" },
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "url",
                                  attrs: { to: { name: "profile" } },
                                },
                                [_vm._v(_vm._s(comment.user.full_name))]
                              ),
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "comment-meta" }, [
                            _c("ul", { staticClass: "global-list" }, [
                              _c(
                                "li",
                                [
                                  _c(
                                    "router-link",
                                    { attrs: { to: { name: "dashboard" } } },
                                    [
                                      _vm._v(
                                        "\n                                    " +
                                          _vm._s(comment.comment_date) +
                                          "\n                                "
                                      ),
                                    ]
                                  ),
                                ],
                                1
                              ),
                            ]),
                          ]),
                        ]),
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("p", [_vm._v(_vm._s(comment.comment))]),
                  _vm._v(" "),
                  _vm.comment_reply == comment.id
                    ? _c(
                        "form",
                        {
                          staticClass: "tr-form mb-4",
                          on: {
                            submit: function ($event) {
                              $event.preventDefault()
                              return _vm.commentReply(comment.id)
                            },
                          },
                        },
                        [
                          _c("div", { staticClass: "row" }, [
                            _c("div", { staticClass: "col-10" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.replyForm.comment,
                                      expression: "replyForm.comment",
                                    },
                                  ],
                                  staticClass: "form-control reply_box",
                                  attrs: {
                                    required: "required",
                                    rows: "2",
                                    placeholder: _vm.lang.write_reply,
                                  },
                                  domProps: { value: _vm.replyForm.comment },
                                  on: {
                                    input: function ($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.replyForm,
                                        "comment",
                                        $event.target.value
                                      )
                                    },
                                  },
                                }),
                              ]),
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-2" },
                              [
                                _vm.loading
                                  ? _c("loading_button", {
                                      attrs: {
                                        class_name: "btn btn-sm btn-primary",
                                      },
                                    })
                                  : _c("input", {
                                      staticClass: "btn btn-primary",
                                      attrs: { type: "submit" },
                                      domProps: { value: _vm.lang.post },
                                    }),
                              ],
                              1
                            ),
                          ]),
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c("div", { staticClass: "comment-icon" }, [
                    _c("ul", { staticClass: "global-list" }, [
                      _vm.alreadyLiked(comment.comment_likes)
                        ? _c("li", [
                            _c(
                              "a",
                              {
                                class: { disable_btn: _vm.blog_like_loading },
                                attrs: { href: "javascript:void(0)" },
                                on: {
                                  click: function ($event) {
                                    return _vm.unLike(comment.id)
                                  },
                                },
                              },
                              [
                                _c("span", { staticClass: "mdi mdi-thumb-up" }),
                                _vm._v(" "),
                                _c("span", { staticClass: "replies_text" }, [
                                  _vm._v(
                                    "(" +
                                      _vm._s(comment.comment_likes.length) +
                                      ")"
                                  ),
                                ]),
                              ]
                            ),
                          ])
                        : _c("li", [
                            _c(
                              "a",
                              {
                                class: { disable_btn: _vm.blog_like_loading },
                                attrs: { href: "javascript:void(0)" },
                                on: {
                                  click: function ($event) {
                                    return _vm.commentLike(comment.id)
                                  },
                                },
                              },
                              [
                                _c("span", {
                                  staticClass: "mdi mdi-thumb-up-outline",
                                }),
                                _vm._v(" "),
                                _c("span", { staticClass: "replies_text" }, [
                                  _vm._v(
                                    "(" +
                                      _vm._s(comment.comment_likes.length) +
                                      ")"
                                  ),
                                ]),
                              ]
                            ),
                          ]),
                      _vm._v(" "),
                      _c("li", [
                        _c(
                          "a",
                          {
                            attrs: { href: "javascript:void(0)" },
                            on: {
                              click: function ($event) {
                                _vm.comment_reply = comment.id
                              },
                            },
                          },
                          [_c("span", { staticClass: "mdi mdi-share" })]
                        ),
                      ]),
                      _vm._v(" "),
                      comment.comment_replies.length > 0
                        ? _c("li", [
                            _c(
                              "a",
                              {
                                attrs: { href: "javascript:void(0)" },
                                on: {
                                  click: function ($event) {
                                    return _vm.showReplyArea(comment.id)
                                  },
                                },
                              },
                              [
                                _c("span", { staticClass: "replies_text" }, [
                                  _vm._v(
                                    _vm._s(comment.comment_replies.length) +
                                      " " +
                                      _vm._s(_vm.lang.replies)
                                  ),
                                ]),
                              ]
                            ),
                          ])
                        : _vm._e(),
                    ]),
                  ]),
                  _vm._v(" "),
                  _c("blog_reply", {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.show_replies == comment.id,
                        expression: "show_replies == comment.id",
                      },
                    ],
                    attrs: {
                      comment: comment,
                      "blog-details": _vm.blogDetails,
                      page: _vm.page,
                    },
                  }),
                ],
                1
              ),
            ])
          }),
          _vm._v(" "),
          _vm.comments.next_page_url
            ? _c("li", [
                _c(
                  "div",
                  { staticClass: "text-center show-more" },
                  [
                    _vm.loading
                      ? _c("loading_button", {
                          attrs: { class_name: "btn btn-primary" },
                        })
                      : _c(
                          "a",
                          {
                            staticClass: "btn btn-primary",
                            attrs: { href: "javascript:void(0)" },
                            on: {
                              click: function ($event) {
                                return _vm.loadMoreComments()
                              },
                            },
                          },
                          [_vm._v(_vm._s(_vm.lang.show_more))]
                        ),
                  ],
                  1
                ),
              ])
            : _vm._e(),
        ],
        2
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/blog_reply.vue?vue&type=template&id=0dd40d9a&":
/*!***************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/blog_reply.vue?vue&type=template&id=0dd40d9a& ***!
  \***************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "ul",
    { staticClass: "children global-list" },
    _vm._l(_vm.comment.comment_replies, function (reply, index) {
      return _c("li", { key: "reply" + index }, [
        _c("div", { staticClass: "comment_info" }, [
          reply.user
            ? _c(
                "div",
                { staticClass: "commenter-avatar" },
                [
                  _c("router-link", { attrs: { to: { name: "dashboard" } } }, [
                    _c("img", {
                      staticClass: "img-fluid",
                      attrs: {
                        src: reply.user.profile_image,
                        alt: reply.user.full_name,
                      },
                    }),
                  ]),
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          reply.user
            ? _c("div", { staticClass: "comment-box" }, [
                _c("div", { staticClass: "comment-title" }, [
                  _c(
                    "span",
                    { staticClass: "title-1" },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "url",
                          attrs: { to: { name: "dashboard" } },
                        },
                        [_vm._v(_vm._s(reply.user.full_name))]
                      ),
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "comment-meta" }, [
                    _c("ul", { staticClass: "global-list" }, [
                      _c(
                        "li",
                        [
                          _c(
                            "router-link",
                            { attrs: { to: { name: "profile" } } },
                            [
                              _vm._v(
                                "\n                                        " +
                                  _vm._s(reply.reply_date) +
                                  "\n                                    "
                              ),
                            ]
                          ),
                        ],
                        1
                      ),
                    ]),
                  ]),
                ]),
              ])
            : _vm._e(),
          _vm._v(" "),
          _c("p", [_vm._v(_vm._s(reply.comment))]),
          _vm._v(" "),
          _vm.replyFormId == reply.id
            ? _c(
                "form",
                {
                  staticClass: "tr-form",
                  on: {
                    submit: function ($event) {
                      $event.preventDefault()
                      return _vm.commentReply(
                        _vm.comment.id,
                        reply.id,
                        reply.level
                      )
                    },
                  },
                },
                [
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-10" }, [
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.replyForm.comment,
                              expression: "replyForm.comment",
                            },
                          ],
                          staticClass: "form-control reply_box",
                          attrs: {
                            required: "required",
                            rows: "2",
                            placeholder: _vm.lang.write_reply,
                          },
                          domProps: { value: _vm.replyForm.comment },
                          on: {
                            input: function ($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.replyForm,
                                "comment",
                                $event.target.value
                              )
                            },
                          },
                        }),
                      ]),
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-2" },
                      [
                        _vm.loading
                          ? _c("loading_button", {
                              attrs: { class_name: "btn btn-primary" },
                            })
                          : _c("input", {
                              staticClass: "btn btn-primary",
                              attrs: { type: "submit" },
                              domProps: { value: _vm.lang.post },
                            }),
                      ],
                      1
                    ),
                  ]),
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "comment-icon" }, [
            _c("ul", { staticClass: "global-list" }, [
              _vm.alreadyLiked(reply.comment_likes)
                ? _c("li", [
                    _c(
                      "a",
                      {
                        class: { disable_btn: _vm.blog_like_loading },
                        attrs: { href: "javaScript:void(0)" },
                        on: {
                          click: function ($event) {
                            return _vm.unLike(reply.id, _vm.comment.id)
                          },
                        },
                      },
                      [
                        _c("span", { staticClass: "mdi mdi-thumb-up" }),
                        _vm._v(" "),
                        _c("span", { staticClass: "replies_text" }, [
                          _vm._v(
                            "(" + _vm._s(reply.comment_likes.length) + ")"
                          ),
                        ]),
                      ]
                    ),
                  ])
                : _c("li", [
                    _c(
                      "a",
                      {
                        class: { disable_btn: _vm.blog_like_loading },
                        attrs: { href: "javaScript:void(0)" },
                        on: {
                          click: function ($event) {
                            return _vm.likeReply(reply.id, _vm.comment.id)
                          },
                        },
                      },
                      [
                        _c("span", { staticClass: "mdi mdi-thumb-up-outline" }),
                        _vm._v(" "),
                        _c("span", { staticClass: "replies_text" }, [
                          _vm._v(
                            "(" + _vm._s(reply.comment_likes.length) + ")"
                          ),
                        ]),
                      ]
                    ),
                  ]),
              _vm._v(" "),
              _c("li", [
                _c(
                  "a",
                  {
                    attrs: { href: "javaScript:void(0)" },
                    on: {
                      click: function ($event) {
                        return _vm.$store.dispatch("replyForm", reply.id)
                      },
                    },
                  },
                  [_c("span", { staticClass: "mdi mdi-share" })]
                ),
              ]),
            ]),
          ]),
        ]),
      ])
    }),
    0
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/nested_reply.vue?vue&type=template&id=4bea6122&":
/*!*****************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/blog_partials/nested_reply.vue?vue&type=template&id=4bea6122& ***!
  \*****************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.reply.replies
    ? _c(
        "ul",
        { staticClass: "children global-list" },
        _vm._l(_vm.reply.replies, function (reply_reply, index) {
          return _c("li", { key: "nested_reply" + index }, [
            _c(
              "div",
              { staticClass: "comment_info" },
              [
                reply_reply.user
                  ? _c(
                      "div",
                      { staticClass: "commenter-avatar" },
                      [
                        _c(
                          "router-link",
                          { attrs: { to: { name: "dashboard" } } },
                          [
                            _c("img", {
                              staticClass: "img-fluid",
                              attrs: {
                                src: reply_reply.user.profile_image,
                                alt: reply_reply.user.full_name,
                              },
                            }),
                          ]
                        ),
                      ],
                      1
                    )
                  : _vm._e(),
                _vm._v(" "),
                reply_reply.user
                  ? _c("div", { staticClass: "comment-box" }, [
                      _c("div", { staticClass: "comment-title" }, [
                        _c(
                          "span",
                          { staticClass: "title-1" },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "url",
                                attrs: { to: { name: "dashboard" } },
                              },
                              [_vm._v(_vm._s(reply_reply.user.full_name))]
                            ),
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "comment-meta" }, [
                          _c("ul", { staticClass: "global-list" }, [
                            _c(
                              "li",
                              [
                                _c(
                                  "router-link",
                                  { attrs: { to: { name: "profile" } } },
                                  [
                                    _vm._v(
                                      "\n                                    " +
                                        _vm._s(reply_reply.reply_date) +
                                        "\n                                "
                                    ),
                                  ]
                                ),
                              ],
                              1
                            ),
                          ]),
                        ]),
                      ]),
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(reply_reply.comment))]),
                _vm._v(" "),
                _vm.replyFormId == reply_reply.id
                  ? _c(
                      "form",
                      {
                        staticClass: "tr-form",
                        on: {
                          submit: function ($event) {
                            $event.preventDefault()
                            return _vm.commentReply(
                              _vm.comment.id,
                              reply_reply.id,
                              reply_reply.level
                            )
                          },
                        },
                      },
                      [
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col-10" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.replyForm.comment,
                                    expression: "replyForm.comment",
                                  },
                                ],
                                staticClass: "form-control reply_box",
                                attrs: {
                                  required: "required",
                                  rows: "2",
                                  placeholder: _vm.lang.write_reply,
                                },
                                domProps: { value: _vm.replyForm.comment },
                                on: {
                                  input: function ($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.replyForm,
                                      "comment",
                                      $event.target.value
                                    )
                                  },
                                },
                              }),
                            ]),
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-2" },
                            [
                              _vm.loading
                                ? _c("loading_button", {
                                    attrs: { class_name: "btn btn-primary" },
                                  })
                                : _c("input", {
                                    staticClass: "btn btn-primary",
                                    attrs: { type: "submit" },
                                    domProps: { value: _vm.lang.post },
                                  }),
                            ],
                            1
                          ),
                        ]),
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _c("div", { staticClass: "comment-icon" }, [
                  _c("ul", { staticClass: "global-list" }, [
                    _vm.alreadyLiked(reply_reply.comment_likes)
                      ? _c("li", [
                          _c(
                            "a",
                            {
                              class: { disable_btn: _vm.blog_like_loading },
                              attrs: { href: "javaScript:void(0)" },
                              on: {
                                click: function ($event) {
                                  return _vm.unLike(
                                    reply_reply.id,
                                    _vm.comment.id
                                  )
                                },
                              },
                            },
                            [
                              _c("span", { staticClass: "mdi mdi-thumb-up" }),
                              _vm._v(" "),
                              _c("span", { staticClass: "replies_text" }, [
                                _vm._v(
                                  "(" +
                                    _vm._s(reply_reply.comment_likes.length) +
                                    ")"
                                ),
                              ]),
                            ]
                          ),
                        ])
                      : _c("li", [
                          _c(
                            "a",
                            {
                              class: { disable_btn: _vm.blog_like_loading },
                              attrs: { href: "javaScript:void(0)" },
                              on: {
                                click: function ($event) {
                                  return _vm.likeReply(
                                    reply_reply.id,
                                    _vm.comment.id
                                  )
                                },
                              },
                            },
                            [
                              _c("span", {
                                staticClass: "mdi mdi-thumb-up-outline",
                              }),
                              _vm._v(" "),
                              _c("span", { staticClass: "replies_text" }, [
                                _vm._v(
                                  "(" +
                                    _vm._s(reply_reply.comment_likes.length) +
                                    ")"
                                ),
                              ]),
                            ]
                          ),
                        ]),
                    _vm._v(" "),
                    _c("li", [
                      _c(
                        "a",
                        {
                          attrs: { href: "javaScript:void(0)" },
                          on: {
                            click: function ($event) {
                              return _vm.$store.dispatch(
                                "replyForm",
                                reply_reply.id
                              )
                            },
                          },
                        },
                        [_c("span", { staticClass: "mdi mdi-share" })]
                      ),
                    ]),
                  ]),
                ]),
                _vm._v(" "),
                _c("nested_reply", {
                  attrs: { reply: reply_reply, comment: _vm.comment },
                }),
              ],
              1
            ),
          ])
        }),
        0
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("img", {
    staticClass: "shimmer",
    style: [_vm.height ? _vm.style : null],
    attrs: {
      src: _vm.getUrl("public/images/default/preview.jpg"),
      alt: "shimmer",
    },
  })
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);