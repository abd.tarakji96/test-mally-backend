"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_frontend_pages_RecipeDetails_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _partials_shimmer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../partials/shimmer */ "./resources/js/components/frontend/partials/shimmer.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import blog_comments from "./blog_partials/blog_comments";

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "blog_details",
  components: {
    //   blog_comments,
    shimmer: _partials_shimmer__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      collapeActive: 'categories',
      commentForm: {
        comment: '',
        blog_id: '',
        slug: this.$route.params.slug
      },
      ss: [],
      page: 1,
      category: true,
      recentPost: true,
      amounts: [],
      cheks: [],
      loading: false,
      share_dropdown: false
    };
  },
  mounted: function mounted() {
    console.log(this.blogDetails, 'pppppfff');
    this.$store.dispatch('recipeDetails', this.$route.params.slug);
    console.log(this.blogDetails, 'pppppfff');
    this.amounts.forEach(function (el) {
      el = 1;
    });
    console.log(this.amounts, 'sajkhdkjsad');
  },
  computed: {
    blogDetails: function blogDetails() {
      return this.$store.getters.getRecipeDetails;
    },
    categories: function categories() {
      return this.$store.getters.getRecipeCategories;
    },
    tags: function tags() {
      return this.$store.getters.getRecipeTags;
    },
    recent_posts: function recent_posts() {
      return this.$store.getters.getRecentPosts;
    },
    ings: function ings() {
      return this.$store.getters.getIngs;
    },
    // comments() {
    //     return this.$store.getters.getBlogComments;
    // },
    shimmer: function shimmer() {
      return this.$store.state.module.shimmer;
    }
  },
  watch: {
    $route: function $route(to, from) {
      this.$store.dispatch('recipeDetails', this.$route.params.slug);
    }
  },
  methods: {
    collapeActiveStatus: function collapeActiveStatus(data) {
      if (this.collapeActive == data) {
        this.collapeActive = '';
      } else {
        this.collapeActive = data;
      }
    },
    comment: function comment() {
      var _this = this;

      this.loading = true;
      this.commentForm.blog_id = this.blogDetails.id;
      var url = this.getUrl('store/blog-comment');
      this.$Progress.start();
      axios.post(url, this.commentForm).then(function (response) {
        _this.loading = false;

        if (response.data.error) {
          toastr.error(response.data.error, _this.lang.Error + ' !!');
        } else {
          _this.commentForm.comment = '';

          _this.$store.dispatch('blogDetails', _this.$route.params.slug);

          if (response.data.success) {
            toastr.success(response.data.success, _this.lang.Success + ' !!');
          }

          _this.$Progress.finish();
        }
      })["catch"](function (error) {
        _this.loading = false;
      });
    },
    loadCategories: function loadCategories() {
      var _this2 = this;

      this.page++;
      var url = this.url + '/load/blog-categories?page=' + this.page;
      this.$Progress.start();
      axios.get(url).then(function (response) {
        if (response.data.error) {
          toastr.error(response.data.error, _this2.lang.Error + ' !!');
        } else {
          var categories = response.data.categories.data;

          if (categories.length > 0) {
            for (var i in categories) {
              _this2.categories.data.push(categories[i]);
            }
          }

          _this2.$Progress.finish();

          _this2.categories.next_page_url = response.data.categories.next_page_url;
        }
      });
    },
    shareDropdown: function shareDropdown() {
      var _this3 = this;

      this.share_dropdown = !this.share_dropdown;
      this.share_dropdown && this.$nextTick(function () {
        document.addEventListener('click', _this3.hideLanguageDropdown);
      });
    },
    getUrl: function getUrl(url) {
      var base_url = document.querySelector('meta[name="base_url"]').getAttribute('content');
      return base_url + '/' + url;
    },
    addTo: function addTo() {
      var _this4 = this;

      console.log(this.cheks, 'cheks');
      console.log(this.amounts, 'amounts');
      this.ings.forEach(function (e, index) {
        if (_this4.cheks[index] == true && _this4.amounts[index] > 0) {
          e.minimum_order_quantity = parseInt(_this4.amounts[index]);

          _this4.cartBtn(e, index);
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "shimmer.vue",
  props: ['height'],
  data: function data() {
    return {
      style: {
        height: this.height + 'px'
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.nn span[data-v-5f1f55f7]{\r\n    color:#005186 !important;\n}\n.post .entry-thumbnail img[data-v-5f1f55f7]{\r\n    border-radius: 10px;\n}\n.post .entry-content[data-v-5f1f55f7] {\r\n    background-color: transparent;\r\n    border-radius: 0;\n}\n.comment-form h3[data-v-5f1f55f7] {\r\n    font-size: 18px;\r\n    margin-bottom: 20px;\r\n    font-weight: 700;\r\n    color: #660f1f;\n}\n.form-control[data-v-5f1f55f7] {\r\n    border: none !important;\r\n    border-radius: 21px;\n}\n.mdi-calendar-month[data-v-5f1f55f7]::before {\r\n    content: none;\n}\r\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_style_index_0_id_5f1f55f7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_style_index_0_id_5f1f55f7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_style_index_0_id_5f1f55f7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./resources/js/components/frontend/pages/RecipeDetails.vue":
/*!******************************************************************!*\
  !*** ./resources/js/components/frontend/pages/RecipeDetails.vue ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _RecipeDetails_vue_vue_type_template_id_5f1f55f7_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true& */ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true&");
/* harmony import */ var _RecipeDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RecipeDetails.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js&");
/* harmony import */ var _RecipeDetails_vue_vue_type_style_index_0_id_5f1f55f7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css& */ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _RecipeDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RecipeDetails_vue_vue_type_template_id_5f1f55f7_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _RecipeDetails_vue_vue_type_template_id_5f1f55f7_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "5f1f55f7",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/pages/RecipeDetails.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/partials/shimmer.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/frontend/partials/shimmer.vue ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shimmer.vue?vue&type=template&id=44ada926& */ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&");
/* harmony import */ var _shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shimmer.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.render,
  _shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/partials/shimmer.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RecipeDetails.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./shimmer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css&":
/*!***************************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css& ***!
  \***************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_style_index_0_id_5f1f55f7_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=style&index=0&id=5f1f55f7&scoped=true&lang=css&");


/***/ }),

/***/ "./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true& ***!
  \*************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_template_id_5f1f55f7_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_template_id_5f1f55f7_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RecipeDetails_vue_vue_type_template_id_5f1f55f7_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true&");


/***/ }),

/***/ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926& ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./shimmer.vue?vue&type=template&id=44ada926& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/RecipeDetails.vue?vue&type=template&id=5f1f55f7&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "sg-page-content" }, [
    _c("section", { staticClass: "grid-view-tab" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _vm.lengthCounter(_vm.blogDetails) > 0
            ? _c("div", { staticClass: "col-md-4 col-lg-3" }, [
                _c("div", { staticClass: "sg-sitebar new-shop-sitebar" }, [
                  _c(
                    "div",
                    {
                      staticClass: "accordion",
                      attrs: { id: "accordionExample" },
                    },
                    [
                      _c("div", { staticClass: "accordion-item" }, [
                        _c(
                          "div",
                          {
                            staticClass: "accordion-header",
                            attrs: { id: "ac1" },
                          },
                          [
                            _c(
                              "button",
                              {
                                staticClass: "accordion-button",
                                class: { collapsed: !_vm.category },
                                attrs: {
                                  type: "button",
                                  "data-bs-toggle": "collapse",
                                  "data-bs-target": "#collapse1",
                                  "aria-expanded": "true",
                                  "aria-controls": "collapse1",
                                },
                                on: {
                                  click: function ($event) {
                                    _vm.category = !_vm.category
                                  },
                                },
                              },
                              [
                                _vm._v(
                                  "All Categories\n                                    "
                                ),
                              ]
                            ),
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "accordion-collapse collapse",
                            class: { show: _vm.category },
                            attrs: {
                              id: "collapse1",
                              "aria-labelledby": "ac1",
                              "data-bs-parent": "#accordionExample",
                            },
                          },
                          [
                            _c("div", { staticClass: "accordion-body" }, [
                              _c(
                                "ul",
                                { staticClass: "global-list" },
                                [
                                  _vm._l(
                                    _vm.categories.data,
                                    function (category, index) {
                                      return _c(
                                        "li",
                                        { key: index },
                                        [
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "category.blogs",
                                                  params: {
                                                    slug: category.slug,
                                                  },
                                                },
                                              },
                                            },
                                            [
                                              _vm._v(
                                                "\n                                                    " +
                                                  _vm._s(category.title) +
                                                  "\n                                                "
                                              ),
                                            ]
                                          ),
                                        ],
                                        1
                                      )
                                    }
                                  ),
                                  _vm._v(" "),
                                  _vm.categories.next_page_url
                                    ? _c("li", [
                                        _c(
                                          "a",
                                          {
                                            attrs: {
                                              href: "javascript:void(0)",
                                            },
                                            on: { click: _vm.loadCategories },
                                          },
                                          [_vm._v(_vm._s(_vm.lang.show_more))]
                                        ),
                                      ])
                                    : _vm._e(),
                                ],
                                2
                              ),
                            ]),
                          ]
                        ),
                      ]),
                      _vm._v(" "),
                      _vm.tags[1] != null
                        ? _c("div", { staticClass: "accordion-item" }, [
                            _c(
                              "div",
                              {
                                staticClass: "accordion-header",
                                attrs: { id: "ac3" },
                              },
                              [
                                _c(
                                  "button",
                                  {
                                    staticClass: "accordion-button",
                                    class: {
                                      collapsed: _vm.collapeActive != "tags",
                                    },
                                    attrs: {
                                      type: "button",
                                      "data-bs-toggle": "collapse",
                                      "data-bs-target": "#collapse3",
                                      "aria-expanded": "false",
                                      "aria-controls": "collapse3",
                                    },
                                    on: {
                                      click: function ($event) {
                                        return _vm.collapeActiveStatus("tags")
                                      },
                                    },
                                  },
                                  [
                                    _vm._v(
                                      _vm._s(_vm.lang.tags) +
                                        "\n                                    "
                                    ),
                                  ]
                                ),
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass: "accordion-collapse collapse",
                                class: { show: _vm.collapeActive == "tags" },
                                attrs: {
                                  id: "collapse3",
                                  "aria-labelledby": "ac3",
                                  "data-bs-parent": "#accordionExample",
                                },
                              },
                              [
                                _c("div", { staticClass: "accordion-body" }, [
                                  _c(
                                    "div",
                                    { staticClass: "tagcloud" },
                                    _vm._l(_vm.tags, function (tag, i) {
                                      return _c(
                                        "a",
                                        {
                                          key: i,
                                          attrs: { href: "javascript:void(0)" },
                                        },
                                        [_vm._v(_vm._s(tag))]
                                      )
                                    }),
                                    0
                                  ),
                                ]),
                              ]
                            ),
                          ])
                        : _vm._e(),
                    ]
                  ),
                ]),
              ])
            : _vm.shimmer
            ? _c(
                "div",
                { staticClass: "col-md-4 col-lg-3" },
                [_c("shimmer", { attrs: { height: 600 } })],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-8 col-lg-9" }, [
            _c("div", { staticClass: "blog-details" }, [
              _c(
                "div",
                {
                  staticClass: "post",
                  staticStyle: {
                    "border-bottom": "1px solid #660F1F",
                    "padding-bottom": "20px",
                  },
                },
                [
                  _c("div", { staticClass: "entry-header" }, [
                    _c("div", { staticClass: "entry-thumbnail" }, [
                      _c(
                        "a",
                        [
                          _vm.blogDetails.banner_img
                            ? _c("img", {
                                staticClass: "img-fluid",
                                attrs: {
                                  loading: "lazy",
                                  src: _vm.blogDetails.banner_img,
                                  alt: _vm.blogDetails.title,
                                },
                              })
                            : _vm.shimmer
                            ? _c("shimmer", { attrs: { height: 200 } })
                            : _vm._e(),
                        ],
                        1
                      ),
                    ]),
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "entry-content" }, [
                    _c("div", { staticClass: "entry-meta" }, [
                      _vm.lengthCounter(_vm.blogDetails) > 0
                        ? _c("ul", { staticClass: "global-list" }, [
                            _vm.blogDetails.user
                              ? _c("li", [
                                  _c(
                                    "a",
                                    { attrs: { href: "javascript:void(0)" } },
                                    [
                                      _vm.blogDetails.user.user_profile_image
                                        ? _c("img", {
                                            staticClass: "img-fluid",
                                            attrs: {
                                              loading: "lazy",
                                              src: _vm.blogDetails.user
                                                .user_profile_image,
                                              alt: _vm.blogDetails.user
                                                .full_name,
                                            },
                                          })
                                        : _vm._e(),
                                      _vm._v(
                                        _vm._s(_vm.blogDetails.user.full_name)
                                      ),
                                    ]
                                  ),
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _c("li", [
                              _c(
                                "a",
                                { attrs: { href: "javascript:void(0)" } },
                                [
                                  _c(
                                    "span",
                                    { staticClass: "mdi mdi-calendar-month" },
                                    [
                                      _c(
                                        "svg",
                                        {
                                          attrs: {
                                            width: "18",
                                            height: "21",
                                            viewBox: "0 0 31 35",
                                            fill: "none",
                                            xmlns: "http://www.w3.org/2000/svg",
                                          },
                                        },
                                        [
                                          _c(
                                            "mask",
                                            {
                                              attrs: {
                                                id: "path-1-inside-1_136_1259",
                                                fill: "white",
                                              },
                                            },
                                            [
                                              _c("path", {
                                                attrs: {
                                                  d: "M31 13.6111V7.77778C31 5.63 29.2652 3.88889 27.125 3.88889H3.875C1.7349 3.88889 0 5.63 0 7.77778V13.6111M31 13.6111V31.1111C31 33.2589 29.2652 35 27.125 35H3.875C1.7349 35 0 33.2589 0 31.1111V13.6111M31 13.6111H0H31ZM7.75 0V7.77778V0ZM23.25 0V7.77778V0Z",
                                                },
                                              }),
                                              _vm._v(" "),
                                              _c("path", {
                                                attrs: {
                                                  d: "M8.71875 17.5H4.84375C4.30872 17.5 3.875 17.9353 3.875 18.4722V22.3611C3.875 22.8981 4.30872 23.3333 4.84375 23.3333H8.71875C9.25378 23.3333 9.6875 22.8981 9.6875 22.3611V18.4722C9.6875 17.9353 9.25378 17.5 8.71875 17.5Z",
                                                },
                                              }),
                                              _vm._v(" "),
                                              _c("path", {
                                                attrs: {
                                                  d: "M17.4375 17.5H13.5625C13.0275 17.5 12.5938 17.9353 12.5938 18.4722V22.3611C12.5938 22.8981 13.0275 23.3333 13.5625 23.3333H17.4375C17.9725 23.3333 18.4062 22.8981 18.4062 22.3611V18.4722C18.4062 17.9353 17.9725 17.5 17.4375 17.5Z",
                                                },
                                              }),
                                              _vm._v(" "),
                                              _c("path", {
                                                attrs: {
                                                  d: "M26.1562 17.5H22.2812C21.7462 17.5 21.3125 17.9353 21.3125 18.4722V22.3611C21.3125 22.8981 21.7462 23.3333 22.2812 23.3333H26.1562C26.6913 23.3333 27.125 22.8981 27.125 22.3611V18.4722C27.125 17.9353 26.6913 17.5 26.1562 17.5Z",
                                                },
                                              }),
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c("path", {
                                            attrs: {
                                              d: "M8.71875 17.5H4.84375C4.30872 17.5 3.875 17.9353 3.875 18.4722V22.3611C3.875 22.8981 4.30872 23.3333 4.84375 23.3333H8.71875C9.25378 23.3333 9.6875 22.8981 9.6875 22.3611V18.4722C9.6875 17.9353 9.25378 17.5 8.71875 17.5Z",
                                              fill: "#660F1F",
                                            },
                                          }),
                                          _vm._v(" "),
                                          _c("path", {
                                            attrs: {
                                              d: "M17.4375 17.5H13.5625C13.0275 17.5 12.5938 17.9353 12.5938 18.4722V22.3611C12.5938 22.8981 13.0275 23.3333 13.5625 23.3333H17.4375C17.9725 23.3333 18.4062 22.8981 18.4062 22.3611V18.4722C18.4062 17.9353 17.9725 17.5 17.4375 17.5Z",
                                              fill: "#660F1F",
                                            },
                                          }),
                                          _vm._v(" "),
                                          _c("path", {
                                            attrs: {
                                              d: "M26.1562 17.5H22.2812C21.7462 17.5 21.3125 17.9353 21.3125 18.4722V22.3611C21.3125 22.8981 21.7462 23.3333 22.2812 23.3333H26.1562C26.6913 23.3333 27.125 22.8981 27.125 22.3611V18.4722C27.125 17.9353 26.6913 17.5 26.1562 17.5Z",
                                              fill: "#660F1F",
                                            },
                                          }),
                                          _vm._v(" "),
                                          _c("path", {
                                            attrs: {
                                              d: "M8.75 0C8.75 -0.552285 8.30228 -1 7.75 -1C7.19771 -1 6.75 -0.552285 6.75 0H8.75ZM6.75 7.77778C6.75 8.33006 7.19771 8.77778 7.75 8.77778C8.30228 8.77778 8.75 8.33006 8.75 7.77778H6.75ZM24.25 0C24.25 -0.552285 23.8023 -1 23.25 -1C22.6977 -1 22.25 -0.552285 22.25 0H24.25ZM22.25 7.77778C22.25 8.33006 22.6977 8.77778 23.25 8.77778C23.8023 8.77778 24.25 8.33006 24.25 7.77778H22.25ZM32 13.6111V7.77778H30V13.6111H32ZM32 7.77778C32 5.0811 29.8208 2.88889 27.125 2.88889V4.88889C28.7095 4.88889 30 6.1789 30 7.77778H32ZM27.125 2.88889H3.875V4.88889H27.125V2.88889ZM3.875 2.88889C1.17924 2.88889 -1 5.08109 -1 7.77778H1C1 6.17891 2.29055 4.88889 3.875 4.88889V2.88889ZM-1 7.77778V13.6111H1V7.77778H-1ZM30 13.6111V31.1111H32V13.6111H30ZM30 31.1111C30 32.71 28.7095 34 27.125 34V36C29.8208 36 32 33.8079 32 31.1111H30ZM27.125 34H3.875V36H27.125V34ZM3.875 34C2.29054 34 1 32.71 1 31.1111H-1C-1 33.8079 1.17925 36 3.875 36V34ZM1 31.1111V13.6111H-1V31.1111H1ZM31 12.6111H0V14.6111H31V12.6111ZM6.75 0V7.77778H8.75V0H6.75ZM22.25 0V7.77778H24.25V0H22.25ZM4.84375 18.5H8.71875V16.5H4.84375V18.5ZM8.71875 18.5C8.71576 18.5 8.71048 18.4992 8.7051 18.4969C8.70039 18.4949 8.69733 18.4925 8.69539 18.4906C8.69345 18.4887 8.69147 18.486 8.68987 18.4822C8.68799 18.4777 8.6875 18.4737 8.6875 18.4722H10.6875C10.6875 17.3864 9.80943 16.5 8.71875 16.5V18.5ZM8.6875 18.4722V22.3611H10.6875V18.4722H8.6875ZM8.6875 22.3611C8.6875 22.3596 8.68799 22.3556 8.68987 22.3512C8.69147 22.3474 8.69345 22.3447 8.69539 22.3427C8.69733 22.3408 8.70039 22.3384 8.7051 22.3364C8.71048 22.3341 8.71576 22.3333 8.71875 22.3333V24.3333C9.80943 24.3333 10.6875 23.447 10.6875 22.3611H8.6875ZM8.71875 22.3333H4.84375V24.3333H8.71875V22.3333ZM4.84375 22.3333C4.84674 22.3333 4.85202 22.3341 4.8574 22.3364C4.86211 22.3384 4.86517 22.3408 4.86711 22.3427C4.86905 22.3447 4.87103 22.3474 4.87263 22.3512C4.87451 22.3556 4.875 22.3596 4.875 22.3611H2.875C2.875 23.447 3.75307 24.3333 4.84375 24.3333V22.3333ZM4.875 22.3611V18.4722H2.875V22.3611H4.875ZM4.875 18.4722C4.875 18.4737 4.87451 18.4777 4.87263 18.4822C4.87103 18.486 4.86905 18.4887 4.86711 18.4906C4.86517 18.4925 4.86211 18.4949 4.8574 18.4969C4.85202 18.4992 4.84674 18.5 4.84375 18.5V16.5C3.75307 16.5 2.875 17.3864 2.875 18.4722H4.875ZM13.5625 18.5H17.4375V16.5H13.5625V18.5ZM17.4375 18.5C17.4345 18.5 17.4292 18.4992 17.4238 18.4969C17.4191 18.4949 17.4161 18.4925 17.4141 18.4906C17.4122 18.4887 17.4102 18.486 17.4086 18.4822C17.4067 18.4777 17.4062 18.4737 17.4062 18.4722H19.4062C19.4062 17.3864 18.5282 16.5 17.4375 16.5V18.5ZM17.4062 18.4722V22.3611H19.4062V18.4722H17.4062ZM17.4062 22.3611C17.4062 22.3596 17.4067 22.3556 17.4086 22.3512C17.4102 22.3474 17.4122 22.3447 17.4141 22.3427C17.4161 22.3408 17.4191 22.3384 17.4238 22.3364C17.4292 22.3341 17.4345 22.3333 17.4375 22.3333V24.3333C18.5282 24.3333 19.4062 23.447 19.4062 22.3611H17.4062ZM17.4375 22.3333H13.5625V24.3333H17.4375V22.3333ZM13.5625 22.3333C13.5655 22.3333 13.5708 22.3341 13.5762 22.3364C13.5809 22.3384 13.5839 22.3408 13.5859 22.3427C13.5878 22.3447 13.5898 22.3474 13.5914 22.3512C13.5933 22.3556 13.5938 22.3596 13.5938 22.3611H11.5938C11.5938 23.447 12.4718 24.3333 13.5625 24.3333V22.3333ZM13.5938 22.3611V18.4722H11.5938V22.3611H13.5938ZM13.5938 18.4722C13.5938 18.4737 13.5933 18.4777 13.5914 18.4822C13.5898 18.486 13.5878 18.4887 13.5859 18.4906C13.5839 18.4925 13.5809 18.4949 13.5762 18.4969C13.5708 18.4992 13.5655 18.5 13.5625 18.5V16.5C12.4718 16.5 11.5938 17.3864 11.5938 18.4722H13.5938ZM22.2812 18.5H26.1562V16.5H22.2812V18.5ZM26.1562 18.5C26.1533 18.5 26.148 18.4992 26.1426 18.4969C26.1379 18.4949 26.1348 18.4925 26.1329 18.4906C26.1309 18.4887 26.129 18.486 26.1274 18.4822C26.1255 18.4777 26.125 18.4737 26.125 18.4722H28.125C28.125 17.3864 27.2469 16.5 26.1562 16.5V18.5ZM26.125 18.4722V22.3611H28.125V18.4722H26.125ZM26.125 22.3611C26.125 22.3596 26.1255 22.3556 26.1274 22.3512C26.129 22.3474 26.1309 22.3447 26.1329 22.3427C26.1348 22.3408 26.1379 22.3384 26.1426 22.3364C26.148 22.3341 26.1533 22.3333 26.1562 22.3333V24.3333C27.2469 24.3333 28.125 23.447 28.125 22.3611H26.125ZM26.1562 22.3333H22.2812V24.3333H26.1562V22.3333ZM22.2812 22.3333C22.2842 22.3333 22.2895 22.3341 22.2949 22.3364C22.2996 22.3384 22.3027 22.3408 22.3046 22.3427C22.3065 22.3447 22.3085 22.3474 22.3101 22.3512C22.312 22.3556 22.3125 22.3596 22.3125 22.3611H20.3125C20.3125 23.447 21.1906 24.3333 22.2812 24.3333V22.3333ZM22.3125 22.3611V18.4722H20.3125V22.3611H22.3125ZM22.3125 18.4722C22.3125 18.4737 22.312 18.4777 22.3101 18.4822C22.3085 18.486 22.3065 18.4887 22.3046 18.4906C22.3027 18.4925 22.2996 18.4949 22.2949 18.4969C22.2895 18.4992 22.2842 18.5 22.2812 18.5V16.5C21.1906 16.5 20.3125 17.3864 20.3125 18.4722H22.3125Z",
                                              fill: "#660F1F",
                                              mask: "url(#path-1-inside-1_136_1259)",
                                            },
                                          }),
                                        ]
                                      ),
                                    ]
                                  ),
                                  _vm._v(
                                    _vm._s(_vm.blogDetails.published_date)
                                  ),
                                ]
                              ),
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("a", { attrs: { href: "#" } }, [
                                _c("span", {
                                  staticClass: "mdi mdi-eye-outline",
                                }),
                                _vm._v(
                                  _vm._s(_vm.blogDetails.blog_views_count) +
                                    " " +
                                    _vm._s(_vm.lang.view)
                                ),
                              ]),
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("div", { staticClass: "dropdown" }, [
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "btn btn-secondary share_dropdown",
                                    class: { show: _vm.share_dropdown },
                                    attrs: {
                                      type: "button",
                                      id: "dropdownMenuButton",
                                      "data-toggle": "dropdown",
                                      "aria-haspopup": "true",
                                      "aria-expanded": "false",
                                    },
                                    on: {
                                      click: function ($event) {
                                        $event.stopPropagation()
                                        return _vm.shareDropdown.apply(
                                          null,
                                          arguments
                                        )
                                      },
                                    },
                                  },
                                  [
                                    _c("span", {
                                      staticClass: "mdi mdi-share-variant",
                                    }),
                                    _vm._v(
                                      _vm._s(_vm.lang.share) +
                                        "\n                                                "
                                    ),
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "dropdown-menu",
                                    class: { show: _vm.share_dropdown },
                                    attrs: {
                                      "aria-labelledby": "dropdownMenuButton",
                                    },
                                    on: {
                                      click: function ($event) {
                                        _vm.share_dropdown = false
                                      },
                                    },
                                  },
                                  [
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          target: "_blank",
                                          href:
                                            "https://www.facebook.com/sharer/sharer.php?u=" +
                                            _vm.getUrl(
                                              "blog/" + _vm.blogDetails.slug
                                            ),
                                        },
                                      },
                                      [_vm._v(_vm._s(_vm.lang.facebook))]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          target: "_blank",
                                          href:
                                            "https://twitter.com/intent/tweet?text=" +
                                            _vm.blogDetails.title +
                                            "&url=" +
                                            _vm.getUrl(
                                              "blog/" + _vm.blogDetails.slug
                                            ),
                                        },
                                      },
                                      [_vm._v(_vm._s(_vm.lang.twitter))]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          target: "_blank",
                                          href:
                                            "https://www.linkedin.com/sharing/share-offsite?mini=true&url=" +
                                            _vm.getUrl(
                                              "blog/" + _vm.blogDetails.slug
                                            ) +
                                            "&title=" +
                                            _vm.blogDetails.title +
                                            "&summary=Extra+linkedin+summary+can+be+passed+here",
                                        },
                                      },
                                      [_vm._v(_vm._s(_vm.lang.linkedin))]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "a",
                                      {
                                        staticClass: "dropdown-item",
                                        attrs: {
                                          target: "_blank",
                                          href:
                                            "https://wa.me/?text=" +
                                            _vm.getUrl(
                                              "blog/" + _vm.blogDetails.slug
                                            ),
                                        },
                                      },
                                      [_vm._v(_vm._s(_vm.lang.whatsapp))]
                                    ),
                                  ]
                                ),
                              ]),
                            ]),
                          ])
                        : _vm.shimmer
                        ? _c(
                            "ul",
                            { staticClass: "global-list" },
                            [_c("shimmer", { attrs: { height: 10 } })],
                            1
                          )
                        : _vm._e(),
                    ]),
                    _vm._v(" "),
                    _vm.lengthCounter(_vm.blogDetails) > 0
                      ? _c("div", [
                          _c("h1", { staticClass: "entry-title" }, [
                            _vm._v(_vm._s(_vm.blogDetails.title)),
                          ]),
                          _vm._v(" "),
                          _c("div", {
                            staticClass: "nn",
                            domProps: {
                              innerHTML: _vm._s(_vm.blogDetails.description),
                            },
                          }),
                        ])
                      : _vm.shimmer
                      ? _c("div", [_c("shimmer", { attrs: { height: 30 } })], 1)
                      : _vm._e(),
                  ]),
                ]
              ),
              _vm._v(" "),
              _vm.lengthCounter(_vm.ings) > 0
                ? _c(
                    "div",
                    {
                      staticStyle: {
                        "margin-top": "20px",
                        "border-bottom": "1px solid #660F1F",
                        "padding-bottom": "40px",
                      },
                    },
                    [
                      _c(
                        "span",
                        {
                          staticStyle: {
                            color: "#66101F",
                            "font-size": "20px",
                            "font-weight": "600",
                          },
                        },
                        [_vm._v("Ingredients List")]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "row mt-3 mb-5" },
                        _vm._l(_vm.ings, function (ing, index) {
                          return _c(
                            "div",
                            {
                              key: ing,
                              staticClass: "col-md-4 mt-3 check",
                              staticStyle: {
                                display: "flex",
                                "align-items": "center",
                                "justify-content": "flex-end",
                                gap: "10px",
                                "flex-direction": "row-reverse",
                              },
                            },
                            [
                              _c(
                                "label",
                                {
                                  staticClass: "register-info-text",
                                  staticStyle: {
                                    margin: "0 6px",
                                    display: "flex",
                                    "align-items": "center",
                                    "flex-direction": "column",
                                  },
                                },
                                [
                                  _c(
                                    "span",
                                    { staticStyle: { color: "#660F1F" } },
                                    [_vm._v(_vm._s(ing.product_name))]
                                  ),
                                  _c(
                                    "span",
                                    { staticStyle: { color: "#660F1F" } },
                                    [_vm._v(_vm._s(ing.price))]
                                  ),
                                ]
                              ),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.amounts[index],
                                    expression: "amounts[index]",
                                  },
                                ],
                                staticStyle: {
                                  height: "55px",
                                  width: "55px",
                                  "border-radius": "10px",
                                  "font-size": "16px",
                                  "background-color": "#FFFAF5",
                                },
                                attrs: { value: "0", min: "0", type: "number" },
                                domProps: { value: _vm.amounts[index] },
                                on: {
                                  input: function ($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.amounts,
                                      index,
                                      $event.target.value
                                    )
                                  },
                                },
                              }),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.cheks[index],
                                    expression: "cheks[index]",
                                  },
                                ],
                                staticStyle: {
                                  height: "20px",
                                  width: "20px",
                                  border: "2px solid #660F1F !important",
                                  "background-color": "#FFFAF5",
                                  "accent-color": "#660F1F",
                                },
                                attrs: { type: "checkbox" },
                                domProps: {
                                  checked: Array.isArray(_vm.cheks[index])
                                    ? _vm._i(_vm.cheks[index], null) > -1
                                    : _vm.cheks[index],
                                },
                                on: {
                                  change: function ($event) {
                                    var $$a = _vm.cheks[index],
                                      $$el = $event.target,
                                      $$c = $$el.checked ? true : false
                                    if (Array.isArray($$a)) {
                                      var $$v = null,
                                        $$i = _vm._i($$a, $$v)
                                      if ($$el.checked) {
                                        $$i < 0 &&
                                          _vm.$set(
                                            _vm.cheks,
                                            index,
                                            $$a.concat([$$v])
                                          )
                                      } else {
                                        $$i > -1 &&
                                          _vm.$set(
                                            _vm.cheks,
                                            index,
                                            $$a
                                              .slice(0, $$i)
                                              .concat($$a.slice($$i + 1))
                                          )
                                      }
                                    } else {
                                      _vm.$set(_vm.cheks, index, $$c)
                                    }
                                  },
                                },
                              }),
                            ]
                          )
                        }),
                        0
                      ),
                      _vm._v(" "),
                      _vm.lengthCounter(_vm.blogDetails) > 0
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-primary",
                              staticStyle: {
                                "border-radius": "10px",
                                height: "40px",
                                "font-size": "11px",
                                padding: "0 24px",
                              },
                              on: {
                                click: function ($event) {
                                  return _vm.addTo()
                                },
                              },
                            },
                            [
                              _c(
                                "svg",
                                {
                                  staticStyle: { "margin-right": "5px" },
                                  attrs: {
                                    width: "17",
                                    height: "17",
                                    viewBox: "0 0 48 44",
                                    fill: "none",
                                    xmlns: "http://www.w3.org/2000/svg",
                                  },
                                },
                                [
                                  _c("path", {
                                    attrs: {
                                      d: "M13.7037 6.41667H46.1667L41.75 21.875H16.082M43.9583 30.7083H17.4583L13.0417 2H6.41667M6.41667 13.0417H2M8.625 19.6667H2M10.8333 26.2917H2M19.6667 39.5417C19.6667 40.7613 18.678 41.75 17.4583 41.75C16.2387 41.75 15.25 40.7613 15.25 39.5417C15.25 38.322 16.2387 37.3333 17.4583 37.3333C18.678 37.3333 19.6667 38.322 19.6667 39.5417ZM43.9583 39.5417C43.9583 40.7613 42.9697 41.75 41.75 41.75C40.5303 41.75 39.5417 40.7613 39.5417 39.5417C39.5417 38.322 40.5303 37.3333 41.75 37.3333C42.9697 37.3333 43.9583 38.322 43.9583 39.5417Z",
                                      stroke: "#FFFAF5",
                                      "stroke-width": "3",
                                      "stroke-linecap": "round",
                                      "stroke-linejoin": "round",
                                    },
                                  }),
                                ]
                              ),
                              _vm._v(
                                "\n                                Add Selected Item To Cart\n                            "
                              ),
                            ]
                          )
                        : _vm._e(),
                    ]
                  )
                : _vm.shimmer
                ? _c("div", [_c("shimmer", { attrs: { height: 30 } })], 1)
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "comment-area" }),
              _vm._v(" "),
              _vm.lengthCounter(_vm.blogDetails) > 0
                ? _c("div", [
                    _vm.authUser
                      ? _c("div", { staticClass: "comment-form" }, [
                          _c("h3", { staticClass: "title-style-1" }, [
                            _vm._v(_vm._s(_vm.lang.write_a_comments)),
                          ]),
                          _vm._v(" "),
                          _c(
                            "form",
                            {
                              staticClass: "tr-form",
                              on: {
                                submit: function ($event) {
                                  $event.preventDefault()
                                  return _vm.comment.apply(null, arguments)
                                },
                              },
                            },
                            [
                              _c("div", { staticClass: "form-group" }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.commentForm.comment,
                                      expression: "commentForm.comment",
                                    },
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    required: "required",
                                    rows: "8",
                                    placeholder: "Write Comment",
                                  },
                                  domProps: { value: _vm.commentForm.comment },
                                  on: {
                                    input: function ($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.commentForm,
                                        "comment",
                                        $event.target.value
                                      )
                                    },
                                  },
                                }),
                              ]),
                              _vm._v(" "),
                              _vm.loading
                                ? _c("loading_button", {
                                    attrs: { class_name: "btn btn-primary" },
                                  })
                                : _c("input", {
                                    staticClass: "btn btn-primary",
                                    attrs: { type: "submit" },
                                    domProps: { value: _vm.lang.post },
                                  }),
                            ],
                            1
                          ),
                        ])
                      : _vm._e(),
                  ])
                : _vm.shimmer
                ? _c(
                    "div",
                    { staticClass: "comment-form" },
                    [_c("shimmer", { attrs: { height: 200 } })],
                    1
                  )
                : _vm._e(),
            ]),
          ]),
        ]),
      ]),
    ]),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("img", {
    staticClass: "shimmer",
    style: [_vm.height ? _vm.style : null],
    attrs: {
      src: _vm.getUrl("public/images/default/preview.jpg"),
      alt: "shimmer",
    },
  })
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);