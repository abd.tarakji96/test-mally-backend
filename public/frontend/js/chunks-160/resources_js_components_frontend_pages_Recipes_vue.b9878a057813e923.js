"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_frontend_pages_Recipes_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/Recipes.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/Recipes.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _partials_shimmer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../partials/shimmer */ "./resources/js/components/frontend/partials/shimmer.vue");
/* harmony import */ var _homepage_Search_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../homepage/Search.vue */ "./resources/js/components/frontend/homepage/Search.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "recipes",
  data: function data() {
    return {
      page: 1,
      activeClass: "",
      form: {
        sort: 'newest',
        slug: this.$route.params.slug,
        title: null
      },
      loading: false,
      is_shimmer: false,
      next_page_url: false
    };
  },
  components: {
    shimmer: _partials_shimmer__WEBPACK_IMPORTED_MODULE_0__["default"],
    Search: _homepage_Search_vue__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  mounted: function mounted() {
    if (this.lengthCounter(this.recipes) == 0) {
      this.allBlogs();
    }

    if (this.lengthCounter(this.recipes) > 0) {
      this.is_shimmer = true;
    }
  },
  computed: {
    recipes: function recipes() {
      return this.$store.getters.getRecipes;
    },
    shimmer: function shimmer() {
      return this.$store.state.module.shimmer;
    }
  },
  watch: {
    $route: function $route(from) {
      if (from.name == 'recipes') {
        this.form.slug = null;
      }

      this.$store.dispatch('recipes', this.form);
    }
  },
  methods: {
    loadMoreData: function loadMoreData() {
      var _this = this;

      this.loading = true;
      this.$Progress.start();
      axios.get(this.next_page_url, {
        params: this.form
      }).then(function (response) {
        if (response.data.error) {
          toastr.error(response.data.error, _this.lang.Error + ' !!');
        } else {
          _this.loading = false;
          var recipes = response.data.recipe.data;

          if (recipes.length > 0) {
            for (var i = 0; i < recipes.length; i++) {
              _this.recipes.data.push(recipes[i]);
            }
          }

          _this.$Progress.finish();
        }

        _this.next_page_url = response.data.recipe.next_page_url;
      });
    },
    filterBlogs: function filterBlogs() {
      this.page = 1;
      this.allBlogs(this.form);
    },
    allBlogs: function allBlogs() {
      var _this2 = this;

      this.loading = true;
      var url = this.getUrl('home/recipes?page=1');
      axios.get(url, {
        params: this.form
      }).then(function (response) {
        console.log(response, 'iiiii');
        _this2.is_shimmer = true;
        _this2.loading = false;

        if (response.data.error) {
          _this2.$Progress.fail();

          toastr.error(response.data.error, _this2.lang.Error + ' !!');
        } else {
          _this2.$store.commit("getRecipes", response.data.recipe);

          _this2.next_page_url = response.data.recipe.next_page_url;
          _this2.page++;

          _this2.$Progress.finish();
        }
      })["catch"](function (error) {
        _this2.loading = false;
        _this2.is_shimmer = true;

        _this2.$Progress.fail();

        if (error.response && error.response.status == 422) {
          toastr.error(response.data.error, _this2.lang.Error + ' !!');
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "shimmer.vue",
  props: ['height'],
  data: function data() {
    return {
      style: {
        height: this.height + 'px'
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/Recipes.vue?vue&type=style&index=0&id=f38824f0&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/Recipes.vue?vue&type=style&index=0&id=f38824f0&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\np[data-v-f38824f0]{\n      color:#005186;\n      font-size: 13px;\n}\n.mm[data-v-f38824f0]{\n    color:#660F1F;\n    position: absolute;\n    bottom: 0;\n    left: 0;\n    margin: 10px;\n}\n.post[data-v-f38824f0]{\n    height:100%;\n    margin-bottom: 15px;\n    border-radius: 4px;\n    background-color: #FFF9F2;\n    border: 1px solid #efefef;\n    text-align: center;\n    position: relative;\n}\n.title[data-v-f38824f0] {\n\tborder-bottom: none;\n\tmargin-bottom: 0;\n}\n.title h1[data-v-f38824f0]{\n\tmargin-bottom: 0;\n}\n.post[data-v-f38824f0] {\n\tmargin-bottom: 30px;\n\tborder: 1px solid #eeeeee;\n\tborder-radius: 10px !important;\n\tbox-shadow: 0px 0px 4px 0px #00000040;\n\tbackground-color: #FFFAF5 !important;\n}\n.post .entry-thumbnail[data-v-f38824f0] {\n\theight: 176px;\n}\n.post .entry-thumbnail img[data-v-f38824f0] {\n\twidth: 100%;\n\theight: 100%;\n\t-o-object-fit: contain;\n\t   object-fit: contain;\n\tborder-radius: 10px 10px 0 0;\n}\n.add_margin[data-v-f38824f0] {\n  margin-top: 15px;\n}\n/* .search_icon :deep() svg rect {\n\tstroke: none;\n  } */\n  ", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/Recipes.vue?vue&type=style&index=0&id=f38824f0&scoped=true&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/Recipes.vue?vue&type=style&index=0&id=f38824f0&scoped=true&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Recipes_vue_vue_type_style_index_0_id_f38824f0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Recipes.vue?vue&type=style&index=0&id=f38824f0&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/Recipes.vue?vue&type=style&index=0&id=f38824f0&scoped=true&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Recipes_vue_vue_type_style_index_0_id_f38824f0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Recipes_vue_vue_type_style_index_0_id_f38824f0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./resources/js/components/frontend/homepage/Search.vue":
/*!**************************************************************!*\
  !*** ./resources/js/components/frontend/homepage/Search.vue ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Search_vue_vue_type_template_id_b0f89352___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Search.vue?vue&type=template&id=b0f89352& */ "./resources/js/components/frontend/homepage/Search.vue?vue&type=template&id=b0f89352&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Search_vue_vue_type_template_id_b0f89352___WEBPACK_IMPORTED_MODULE_0__.render,
  _Search_vue_vue_type_template_id_b0f89352___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/homepage/Search.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/pages/Recipes.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/frontend/pages/Recipes.vue ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Recipes_vue_vue_type_template_id_f38824f0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Recipes.vue?vue&type=template&id=f38824f0&scoped=true& */ "./resources/js/components/frontend/pages/Recipes.vue?vue&type=template&id=f38824f0&scoped=true&");
/* harmony import */ var _Recipes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Recipes.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/pages/Recipes.vue?vue&type=script&lang=js&");
/* harmony import */ var _Recipes_vue_vue_type_style_index_0_id_f38824f0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Recipes.vue?vue&type=style&index=0&id=f38824f0&scoped=true&lang=css& */ "./resources/js/components/frontend/pages/Recipes.vue?vue&type=style&index=0&id=f38824f0&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Recipes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Recipes_vue_vue_type_template_id_f38824f0_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _Recipes_vue_vue_type_template_id_f38824f0_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "f38824f0",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/pages/Recipes.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/partials/shimmer.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/frontend/partials/shimmer.vue ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shimmer.vue?vue&type=template&id=44ada926& */ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&");
/* harmony import */ var _shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shimmer.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.render,
  _shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/partials/shimmer.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/pages/Recipes.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/Recipes.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Recipes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Recipes.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/Recipes.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Recipes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./shimmer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/pages/Recipes.vue?vue&type=style&index=0&id=f38824f0&scoped=true&lang=css&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/Recipes.vue?vue&type=style&index=0&id=f38824f0&scoped=true&lang=css& ***!
  \*********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_8_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Recipes_vue_vue_type_style_index_0_id_f38824f0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Recipes.vue?vue&type=style&index=0&id=f38824f0&scoped=true&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-8[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/Recipes.vue?vue&type=style&index=0&id=f38824f0&scoped=true&lang=css&");


/***/ }),

/***/ "./resources/js/components/frontend/homepage/Search.vue?vue&type=template&id=b0f89352&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/frontend/homepage/Search.vue?vue&type=template&id=b0f89352& ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Search_vue_vue_type_template_id_b0f89352___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Search_vue_vue_type_template_id_b0f89352___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Search_vue_vue_type_template_id_b0f89352___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Search.vue?vue&type=template&id=b0f89352& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/homepage/Search.vue?vue&type=template&id=b0f89352&");


/***/ }),

/***/ "./resources/js/components/frontend/pages/Recipes.vue?vue&type=template&id=f38824f0&scoped=true&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/components/frontend/pages/Recipes.vue?vue&type=template&id=f38824f0&scoped=true& ***!
  \*******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Recipes_vue_vue_type_template_id_f38824f0_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Recipes_vue_vue_type_template_id_f38824f0_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Recipes_vue_vue_type_template_id_f38824f0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Recipes.vue?vue&type=template&id=f38824f0&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/Recipes.vue?vue&type=template&id=f38824f0&scoped=true&");


/***/ }),

/***/ "./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926& ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_shimmer_vue_vue_type_template_id_44ada926___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./shimmer.vue?vue&type=template&id=44ada926& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/homepage/Search.vue?vue&type=template&id=b0f89352&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/homepage/Search.vue?vue&type=template&id=b0f89352& ***!
  \************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "svg",
    {
      attrs: {
        width: "30",
        height: "30",
        viewBox: "0 0 57 57",
        fill: "none",
        xmlns: "http://www.w3.org/2000/svg",
      },
    },
    [
      _c("rect", {
        attrs: {
          x: "0.540039",
          y: "0.528809",
          width: "55",
          height: "55",
          rx: "9.5",
          stroke: "#660F1F",
        },
      }),
      _vm._v(" "),
      _c("path", {
        attrs: {
          d: "M23.3109 17.7249C20.6038 17.7249 18.4014 19.9287 18.4014 22.6374C18.4014 23.0137 18.7062 23.3187 19.0822 23.3187C19.4583 23.3187 19.7631 23.0137 19.7631 22.6374C19.7631 20.68 21.3546 19.0875 23.3109 19.0875C23.687 19.0875 23.9918 18.7825 23.9918 18.4062C23.9918 18.0299 23.687 17.7249 23.3109 17.7249Z",
          fill: "#660F1F",
        },
      }),
      _vm._v(" "),
      _c("path", {
        attrs: {
          d: "M35.3027 30.3559C36.5335 28.4251 37.2467 26.1344 37.2467 23.6795C37.2467 16.8136 31.6652 11.2288 24.8034 11.2288C22.3499 11.2288 20.0605 11.9425 18.1309 13.174C16.5935 14.1547 15.2851 15.4638 14.305 17.0021C13.0742 18.9329 12.36 21.2245 12.36 23.6795C12.36 30.5445 17.9424 36.1301 24.8034 36.1301C27.2569 36.1301 29.5472 35.4156 31.4768 34.184C33.0142 33.2033 34.3225 31.8942 35.3027 30.3559ZM28.8384 31.5442C27.6284 32.1685 26.256 32.5213 24.8034 32.5213C19.9316 32.5213 15.9668 28.5542 15.9668 23.6795C15.9668 22.226 16.3194 20.8528 16.9433 19.6421C17.7882 18.0018 19.1309 16.6593 20.7701 15.8148C21.9802 15.1905 23.3516 14.8377 24.8033 14.8377C29.6761 14.8377 33.6399 18.8039 33.6399 23.6795C33.6399 25.1321 33.2873 26.5044 32.6633 27.7151C31.8194 29.3554 30.4777 30.6988 28.8384 31.5442Z",
          fill: "#660F1F",
        },
      }),
      _vm._v(" "),
      _c("path", {
        attrs: {
          d: "M44.0481 39.0876L36.8644 31.9177C35.8418 33.4118 34.5317 34.7217 33.0385 35.7458L40.2222 42.9158C40.7506 43.4445 41.4432 43.7088 42.1348 43.7088C42.8272 43.7088 43.5198 43.4445 44.0482 42.9158C45.104 41.8583 45.104 40.145 44.0481 39.0876Z",
          fill: "#660F1F",
        },
      }),
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/Recipes.vue?vue&type=template&id=f38824f0&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/pages/Recipes.vue?vue&type=template&id=f38824f0&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "section",
    { staticClass: "sg-blog-section sg-filter", class: _vm.activeClass },
    [
      _c("div", { staticClass: "container" }, [
        _c(
          "div",
          { staticClass: "title blog-header justify-content-between" },
          [
            _c("h1", [_vm._v("Recipes")]),
            _vm._v(" "),
            _c("div", { staticClass: "right-content" }, [
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.sort,
                      expression: "form.sort",
                    },
                  ],
                  staticClass: "form-control",
                  on: {
                    change: [
                      function ($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function (o) {
                            return o.selected
                          })
                          .map(function (o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.form,
                          "sort",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      },
                      _vm.filterBlogs,
                    ],
                  },
                },
                [
                  _c("option", { attrs: { value: "newest" } }, [
                    _vm._v(_vm._s(_vm.lang.newest)),
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "oldest" } }, [
                    _vm._v(_vm._s(_vm.lang.oldest)),
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "viewed" } }, [
                    _vm._v(_vm._s(_vm.lang.most_viewed)),
                  ]),
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "d-flex gap-3" }, [
                _c("div", { staticClass: "sg-search" }, [
                  _c("div", { staticClass: "search-form blog-search" }, [
                    _c(
                      "form",
                      {
                        on: {
                          submit: function ($event) {
                            $event.preventDefault()
                            return _vm.filterBlogs.apply(null, arguments)
                          },
                        },
                      },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.title,
                              expression: "form.title",
                            },
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: _vm.lang.search_blog,
                          },
                          domProps: { value: _vm.form.title },
                          on: {
                            input: function ($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.form, "title", $event.target.value)
                            },
                          },
                        }),
                        _vm._v(" "),
                        _vm.loading
                          ? _c("loading_button")
                          : _c("button", { attrs: { type: "submit" } }, [
                              _c(
                                "span",
                                { staticClass: "mdi mdi-name mdi-magnify" },
                                [
                                  _c(
                                    "svg",
                                    {
                                      attrs: {
                                        width: "20",
                                        height: "20",
                                        viewBox: "0 0 32 32",
                                        fill: "none",
                                        xmlns: "http://www.w3.org/2000/svg",
                                      },
                                    },
                                    [
                                      _c("path", {
                                        attrs: {
                                          d: "M14.2 6.04998C18.7563 6.04998 22.45 9.74363 22.45 14.3M23.537 23.6306L30.7 30.8M27.4 14.3C27.4 21.5902 21.4902 27.5 14.2 27.5C6.90984 27.5 1 21.5902 1 14.3C1 7.00981 6.90984 1.09998 14.2 1.09998C21.4902 1.09998 27.4 7.00981 27.4 14.3Z",
                                          stroke: "#660F1F",
                                          "stroke-width": "2",
                                          "stroke-linecap": "round",
                                          "stroke-linejoin": "round",
                                        },
                                      }),
                                    ]
                                  ),
                                ]
                              ),
                            ]),
                      ],
                      1
                    ),
                  ]),
                ]),
                _vm._v(" "),
                _c("ul", { staticClass: "filter-tabs global-list" }, [
                  _c(
                    "li",
                    {
                      staticClass: "grid-view-tab",
                      class: {
                        active:
                          _vm.activeClass == "grid-view-tab" ||
                          _vm.activeClass == "",
                      },
                      on: {
                        click: function ($event) {
                          _vm.activeClass = "grid-view-tab"
                        },
                      },
                    },
                    [_c("span", { staticClass: "mdi mdi-name mdi-grid" })]
                  ),
                  _vm._v(" "),
                  _c(
                    "li",
                    {
                      staticClass: "list-view-tab",
                      class: { active: _vm.activeClass == "list-view-tab" },
                      on: {
                        click: function ($event) {
                          _vm.activeClass = "list-view-tab"
                        },
                      },
                    },
                    [
                      _c("span", {
                        staticClass: "mdi mdi-name mdi-format-list-bulleted",
                      }),
                    ]
                  ),
                ]),
              ]),
            ]),
          ]
        ),
        _vm._v(" "),
         true && _vm.is_shimmer
          ? _c(
              "div",
              { staticClass: "row" },
              _vm._l(_vm.recipes.data, function (blog, i) {
                return _c(
                  "div",
                  { key: i, staticClass: "col-md-6 col-lg-3 add_margin" },
                  [
                    _c("div", { staticClass: "post" }, [
                      _c("div", { staticClass: "entry-header" }, [
                        _c(
                          "div",
                          { staticClass: "entry-thumbnail" },
                          [
                            _c(
                              "router-link",
                              {
                                attrs: {
                                  to: {
                                    name: "recipe.details",
                                    params: { slug: blog.slug },
                                  },
                                },
                              },
                              [
                                _c("img", {
                                  staticClass: "img-fluid",
                                  attrs: {
                                    loading: "lazy",
                                    src: blog.thumbnail,
                                    alt: blog.title,
                                  },
                                }),
                              ]
                            ),
                          ],
                          1
                        ),
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "entry-content" },
                        [
                          _c(
                            "router-link",
                            {
                              attrs: {
                                to: {
                                  name: "recipe.details",
                                  params: { slug: blog.slug },
                                },
                              },
                            },
                            [
                              _c(
                                "h1",
                                { staticClass: "entry-title text-ellipse" },
                                [_vm._v(_vm._s(blog.title))]
                              ),
                            ]
                          ),
                          _vm._v(" "),
                          _c("p", { staticClass: "inner_recipe" }, [
                            _vm._v(_vm._s(blog.short_description)),
                          ]),
                          _vm._v(" "),
                          _c(
                            "router-link",
                            {
                              staticClass: "mm",
                              attrs: {
                                to: {
                                  name: "recipe.details",
                                  params: { slug: blog.slug },
                                },
                              },
                            },
                            [
                              _vm._v(
                                "\n                  " +
                                  _vm._s(_vm.lang.read_more) +
                                  "\n                "
                              ),
                            ]
                          ),
                        ],
                        1
                      ),
                    ]),
                  ]
                )
              }),
              0
            )
          : _vm.shimmer
          ? _c(
              "div",
              { staticClass: "row" },
              _vm._l(12, function (blog, i) {
                return _c(
                  "div",
                  { key: i, staticClass: "col-md-6 col-lg-3 add_margin" },
                  [_c("div", { staticClass: "post" }, [_c("shimmer")], 1)]
                )
              }),
              0
            )
          : _vm._e(),
        _vm._v(" "),
        _vm.next_page_url && !_vm.loading
          ? _c("div", { staticClass: "show-more mt-4" }, [
              _c(
                "a",
                {
                  staticClass: "btn btn-primary",
                  attrs: { href: "javaScript:void(0)" },
                  on: {
                    click: function ($event) {
                      return _vm.loadMoreData()
                    },
                  },
                },
                [_vm._v(_vm._s(_vm.lang.show_more))]
              ),
            ])
          : _vm._e(),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.loading,
                expression: "loading",
              },
            ],
            staticClass: "col-md-12 text-center show-more",
          },
          [_c("loading_button", { attrs: { class_name: "btn btn-primary" } })],
          1
        ),
      ]),
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/frontend/partials/shimmer.vue?vue&type=template&id=44ada926& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("img", {
    staticClass: "shimmer",
    style: [_vm.height ? _vm.style : null],
    attrs: {
      src: _vm.getUrl("public/images/default/preview.jpg"),
      alt: "shimmer",
    },
  })
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);