
@push('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.customer-by-ajax').select2({
                multiple: "multiple",
                placeholder: "{{ __('Select Customer') }}",
                ajax: {
                    type: "GET",
                    dataType: 'json',
                    url: '{{ route('user.by.ajax') }}',

                    data: function (params) {

                        return {
                            q: params.term // search term
                        };
                    },
                    delay: 250,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });
        });
    </script>
@endpush
