@extends('admin.partials.master')
@section('title')
    {{ __('notifications') }}
@endsection
@section('marketing_active')
    active
@endsection
@section('coupon')
    active
@endsection
@section('main-content')
    <section class="section">
        <div class="section-body">
            <div class="d-flex justify-content-between">
                <div class="d-block">
                    <h2 class="section-title">{{ __('All Coupon') }}</h2>
                    <p class="section-lead">
                        {{ __('You have total') . ' ' . count($notifications) . ' ' . __('notifications') }}
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div
                class="{{ hasPermission('coupon_create') ? 'col-sm-xs-12 col-md-7' : 'col-sm-xs-12 col-md-10 middle' }}">
                <div class="card">
                    <form action="">
                        <div class="card-header input-title">
                            <h4>{{ __('notifications') }}</h4>
                        </div>
                    </form>
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped table-md">
                                <tbody>
                                <tr>
                                    <th>#</th>
                                    <th>{{ __('Title') }}</th>
                                    <th>{{ __('Deiscription') }}</th>
                                    <th>{{ __('Type') }}</th>
                                    <th>{{ __('Notification Count') }}</th>
                                  
                                    @if(hasPermission('coupon_delete'))
                                        <th>{{ __('Options') }}</th>
                                    @endif
                                </tr>
                                @foreach ($notifications as $key => $notification)
                                    <tr id="row_{{ $notification->id }}" class="table-data-row">
                                        <input type="hidden" value="{{$notification->id}}" id="id">
                                        <td>{{ 1 + $key  }}</td>
                                        <td>
                                            {{ $notification->title }}
                                        </td>
                                        <td>
                                            {{ $notification->type }}
                                        </td>
                                        <td>
                                            {{ $notification->description }}
                                        </td>
                                        <td>
                                            {{ $notification->notification_count }}
                                        
                                        </td>
                                                              
                                        
                                        <td>

                                            @if(hasPermission('coupon_delete'))
                                                <form action="{{ route('notification.delete', ['id' => $notification->id]) }}" method="POST">
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-outline-danger btn-circle">
                                                        <i class='bx bx-trash'></i>
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <nav class="d-inline-block">
                            {{--  {{ $notifications->appends(Request::except('page'))->links('pagination::bootstrap-4') }}  --}}
                        </nav>
                    </div>
                </div>
            </div>
            @if(hasPermission('coupon_create'))
                <div class="col-sm-xs-12 col-md-5">
                    <div class="card">
                        <div class="card-header input-title">
                            <h4>{{ __('Add Notification') }}</h4>
                        </div>
                        <div class="card-body card-body-paddding">
                            <form method="POST" action="{{ route('notification.store') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="title">{{ __('Title') }} *</label>
                                    <input type="text" name="title" id="title" value="{{ old('title') }}" class="form-control" >
                                    @if ($errors->has('title'))
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('title') }}</p>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="title">{{ __('Descrtiption') }} *</label>
                                    <input type="text" name="description" id="description" value="{{ old('description') }}" class="form-control" >
                                    @if ($errors->has('description'))
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('description') }}</p>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="notificationType">{{ __('Notification Type') }} *</label>
                                    <div class="custom-file">
                                        <select class="form-control selectric" name="type" id="notificationType" >
                                            <option
                                                {{ old('type') ? (old('type') == "all" ? "selected" : "selected") : "" }} value="all">{{ __('All') }}</option>
                                            <option
                                                 {{ old('type') ? (old('type') == "users" ? "selected" : "selected") : "" }} value="users">{{ __('users') }}</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('type'))
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('type') }}</p>
                                        </div>
                                    @endif
                                </div>
                               
                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-outline-primary" tabindex="4">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
    @include('admin.common.selector-modal')
@endsection

@section('page-style')
    <link rel="stylesheet" href="{{ static_asset('admin/css/daterangepicker.css') }}">
@endsection

@push('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.product-by-ajax').select2({
                placeholder: "{{ __('Select Product') }}",
                minimumInputLength: 2,
                closeOnSelect: true,
                'multiple': true,
                ajax: {
                    type: "GET",
                    dataType: 'json',
                    url: '{{ route('product.by.ajax') }}',
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    delay: 250,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });
        });
    </script>
@endpush
@section('style')
    <link rel="stylesheet" href="{{ static_asset('admin/css/dropzone.css') }}">
@endsection
@push('script')
    <script type="text/javascript" src="{{ static_asset('admin/js/dropzone.min.js') }}"></script>
@endpush
